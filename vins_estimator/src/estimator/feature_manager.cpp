/*******************************************************
 * Copyright (C) 2019, Aerial Robotics Group, Hong Kong University of Science and Technology
 *
 * This file is part of VINS.
 *
 * Licensed under the GNU General Public License v3.0;
 * you may not use this file except in compliance with the License.
 *******************************************************/

#include "feature_manager.h"

int FeaturePerId::endFrame()
{
    return start_frame + feature_per_frame.size() - 1;
}

int LineFeaturePerId::endFrame()
{
    return start_frame + line_feature_per_frame.size() - 1;
}

FeatureManager::FeatureManager(Matrix3d _Rs[])
    : Rs(_Rs)
{
    for (int i = 0; i < NUM_OF_CAM; i++)
        ric[i].setIdentity();
}

void FeatureManager::setRic(Matrix3d _ric[])
{
    for (int i = 0; i < NUM_OF_CAM; i++)
    {
        ric[i] = _ric[i];
    }
}

void FeatureManager::clearState()
{
    feature.clear();
}

int FeatureManager::getFeatureCount()
{
    int cnt = 0;
    for (auto &it : feature)
    {
        it.used_num = it.feature_per_frame.size();
        if (it.used_num >= 4)
        {
            cnt++;
        }
    }
    return cnt;
}


bool FeatureManager::addFeatureCheckParallax(int frame_count,
                                             const map<int, vector<pair<int, Eigen::Matrix<double, 7, 1>>>> &image,
                                             const map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> &image_line,
                                             double td)
{
    ROS_DEBUG("input feature: %d", (int)image.size());
    ROS_DEBUG("num of feature: %d", getFeatureCount());
    double parallax_sum = 0;
    int parallax_num = 0;
    last_track_num = 0;
    last_average_parallax = 0;
    new_feature_num = 0;
    long_track_num = 0;
    for (auto &id_pts : image)
    {
        FeaturePerFrame f_per_fra(id_pts.second[0].second, td);
        assert(id_pts.second[0].first == 0);
        if(id_pts.second.size() == 2)
        {
            f_per_fra.rightObservation(id_pts.second[1].second);
            assert(id_pts.second[1].first == 1);
        }

        int feature_id = id_pts.first;
        auto it = find_if(feature.begin(), feature.end(), [feature_id](const FeaturePerId &it)
                          {
            return it.feature_id == feature_id;
                          });

        if (it == feature.end())
        {
            feature.push_back(FeaturePerId(feature_id, frame_count));
            feature.back().feature_per_frame.push_back(f_per_fra);
            new_feature_num++;
        }
        else if (it->feature_id == feature_id)
        {
            it->feature_per_frame.push_back(f_per_fra);
            last_track_num++;
            if( it-> feature_per_frame.size() >= 4)
                long_track_num++;
        }
    }

    unsigned int new_line = 0;
    unsigned int old_line = 0;
    for (auto &id_lines : image_line)
    {
        LineFeaturePerFrame l_per_fra(id_lines.second[0].second, td);

        int line_id = id_lines.first;
        auto it = find_if(line_feature.begin(), line_feature.end(), [line_id](const LineFeaturePerId &it)
        {
            return it.feature_id == line_id;
        });

        if (it == line_feature.end())
        {
            line_feature.push_back(LineFeaturePerId(line_id, frame_count));
            line_feature.back().line_feature_per_frame.push_back(l_per_fra);
//            cout << line_id << ", " << id_lines.second[0].first << endl;
            new_line++;

        }
        else if (it->feature_id == line_id)
        {
            it->line_feature_per_frame.push_back(l_per_fra);
            // ADDED CODE
            it->max_num = it->max_num+1;
            old_line++;
        }
    }

//    cout << new_line << ", " << old_line << endl;

    //if (frame_count < 2 || last_track_num < 20)
    //if (frame_count < 2 || last_track_num < 20 || new_feature_num > 0.5 * last_track_num)
    if (frame_count < 2 || last_track_num < 20 || long_track_num < 40 || new_feature_num > 0.5 * last_track_num)
        return true;

    for (auto &it_per_id : feature)
    {
        if (it_per_id.start_frame <= frame_count - 2 &&
            it_per_id.start_frame + int(it_per_id.feature_per_frame.size()) - 1 >= frame_count - 1)
        {
            parallax_sum += compensatedParallax2(it_per_id, frame_count);
            parallax_num++;
        }
    }

    if (parallax_num == 0)
    {
        return true;
    }
    else
    {
        ROS_DEBUG("parallax_sum: %lf, parallax_num: %d", parallax_sum, parallax_num);
        ROS_DEBUG("current parallax: %lf", parallax_sum / parallax_num * FOCAL_LENGTH);
        last_average_parallax = parallax_sum / parallax_num * FOCAL_LENGTH;
        return parallax_sum / parallax_num >= MIN_PARALLAX;
    }
}

vector<pair<Vector3d, Vector3d>> FeatureManager::getCorresponding(int frame_count_l, int frame_count_r)
{
    vector<pair<Vector3d, Vector3d>> corres;
    for (auto &it : feature)
    {
        if (it.start_frame <= frame_count_l && it.endFrame() >= frame_count_r)
        {
            Vector3d a = Vector3d::Zero(), b = Vector3d::Zero();
            int idx_l = frame_count_l - it.start_frame;
            int idx_r = frame_count_r - it.start_frame;

            a = it.feature_per_frame[idx_l].point;

            b = it.feature_per_frame[idx_r].point;

            corres.push_back(make_pair(a, b));
        }
    }
    return corres;
}

void FeatureManager::setDepth(const VectorXd &x)
{
    int feature_index = -1;
    for (auto &it_per_id : feature)
    {
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (it_per_id.used_num < 4)
            continue;

        it_per_id.estimated_depth = 1.0 / x(++feature_index);
        //ROS_INFO("feature id %d , start_frame %d, depth %f ", it_per_id->feature_id, it_per_id-> start_frame, it_per_id->estimated_depth);
        if (it_per_id.estimated_depth < 0)
        {
            it_per_id.solve_flag = 2;
        }
        else
            it_per_id.solve_flag = 1;
    }
}

void FeatureManager::removeFailures()
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;
        if (it->solve_flag == 2)
            feature.erase(it);
    }
}

void FeatureManager::clearDepth()
{
    for (auto &it_per_id : feature)
        it_per_id.estimated_depth = -1;
}

VectorXd FeatureManager::getDepthVector()
{
    VectorXd dep_vec(getFeatureCount());
    int feature_index = -1;
    for (auto &it_per_id : feature)
    {
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (it_per_id.used_num < 4)
            continue;
#if 1
        dep_vec(++feature_index) = 1. / it_per_id.estimated_depth;
#else
        dep_vec(++feature_index) = it_per_id->estimated_depth;
#endif
    }
    return dep_vec;
}


void FeatureManager::triangulatePoint(Eigen::Matrix<double, 3, 4> &Pose0, Eigen::Matrix<double, 3, 4> &Pose1,
                        Eigen::Vector2d &point0, Eigen::Vector2d &point1, Eigen::Vector3d &point_3d)
{
    Eigen::Matrix4d design_matrix = Eigen::Matrix4d::Zero();
    design_matrix.row(0) = point0[0] * Pose0.row(2) - Pose0.row(0);
    design_matrix.row(1) = point0[1] * Pose0.row(2) - Pose0.row(1);
    design_matrix.row(2) = point1[0] * Pose1.row(2) - Pose1.row(0);
    design_matrix.row(3) = point1[1] * Pose1.row(2) - Pose1.row(1);
    Eigen::Vector4d triangulated_point;
    triangulated_point =
              design_matrix.jacobiSvd(Eigen::ComputeFullV).matrixV().rightCols<1>();
    point_3d(0) = triangulated_point(0) / triangulated_point(3);
    point_3d(1) = triangulated_point(1) / triangulated_point(3);
    point_3d(2) = triangulated_point(2) / triangulated_point(3);
}


bool FeatureManager::solvePoseByPnP(Eigen::Matrix3d &R, Eigen::Vector3d &P,
                                      vector<cv::Point2f> &pts2D, vector<cv::Point3f> &pts3D)
{
    Eigen::Matrix3d R_initial;
    Eigen::Vector3d P_initial;

    // w_T_cam ---> cam_T_w
    R_initial = R.inverse();
    P_initial = -(R_initial * P);

    //printf("pnp size %d \n",(int)pts2D.size() );
    if (int(pts2D.size()) < 4)
    {
        printf("feature tracking not enough, please slowly move you device! \n");
        return false;
    }
    cv::Mat r, rvec, t, D, tmp_r;
    cv::eigen2cv(R_initial, tmp_r);
    cv::Rodrigues(tmp_r, rvec);
    cv::eigen2cv(P_initial, t);
    cv::Mat K = (cv::Mat_<double>(3, 3) << 1, 0, 0, 0, 1, 0, 0, 0, 1);
    bool pnp_succ;
    pnp_succ = cv::solvePnP(pts3D, pts2D, K, D, rvec, t, 1);
    //pnp_succ = solvePnPRansac(pts3D, pts2D, K, D, rvec, t, true, 100, 8.0 / focalLength, 0.99, inliers);

    if(!pnp_succ)
    {
        printf("pnp failed ! \n");
        return false;
    }
    cv::Rodrigues(rvec, r);
    //cout << "r " << endl << r << endl;
    Eigen::MatrixXd R_pnp;
    cv::cv2eigen(r, R_pnp);
    Eigen::MatrixXd T_pnp;
    cv::cv2eigen(t, T_pnp);

    // cam_T_w ---> w_T_cam
    R = R_pnp.transpose();
    P = R * (-T_pnp);

    return true;
}

void FeatureManager::initFramePoseByPnP(int frameCnt, Vector3d Ps[], Matrix3d Rs[], Vector3d tic[], Matrix3d ric[])
{

    if(frameCnt > 0)
    {
        vector<cv::Point2f> pts2D;
        vector<cv::Point3f> pts3D;
        for (auto &it_per_id : feature)
        {
            if (it_per_id.estimated_depth > 0)
            {
                int index = frameCnt - it_per_id.start_frame;
                if((int)it_per_id.feature_per_frame.size() >= index + 1)
                {
                    Vector3d ptsInCam = ric[0] * (it_per_id.feature_per_frame[0].point * it_per_id.estimated_depth) + tic[0];
                    Vector3d ptsInWorld = Rs[it_per_id.start_frame] * ptsInCam + Ps[it_per_id.start_frame];

                    cv::Point3f point3d(ptsInWorld.x(), ptsInWorld.y(), ptsInWorld.z());
                    cv::Point2f point2d(it_per_id.feature_per_frame[index].point.x(), it_per_id.feature_per_frame[index].point.y());
                    pts3D.push_back(point3d);
                    pts2D.push_back(point2d);
                }
            }
        }
        Eigen::Matrix3d RCam;
        Eigen::Vector3d PCam;
        // trans to w_T_cam
        RCam = Rs[frameCnt - 1] * ric[0];
        PCam = Rs[frameCnt - 1] * tic[0] + Ps[frameCnt - 1];

        if(solvePoseByPnP(RCam, PCam, pts2D, pts3D))
        {
            // trans to w_T_imu
            Rs[frameCnt] = RCam * ric[0].transpose();
            Ps[frameCnt] = -RCam * ric[0].transpose() * tic[0] + PCam;

            Eigen::Quaterniond Q(Rs[frameCnt]);
            //cout << "frameCnt: " << frameCnt <<  " pnp Q " << Q.w() << " " << Q.vec().transpose() << endl;
            //cout << "frameCnt: " << frameCnt << " pnp P " << Ps[frameCnt].transpose() << endl;
        }
    }
}

void FeatureManager::triangulate(int frameCnt, Vector3d Ps[], Matrix3d Rs[], Vector3d tic[], Matrix3d ric[])
{
    for (auto &it_per_id : feature)
    {
        if (it_per_id.estimated_depth > 0)
            continue;

        if(STEREO && it_per_id.feature_per_frame[0].is_stereo)
        {
            int imu_i = it_per_id.start_frame;
            Eigen::Matrix<double, 3, 4> leftPose;
            Eigen::Vector3d t0 = Ps[imu_i] + Rs[imu_i] * tic[0];
            Eigen::Matrix3d R0 = Rs[imu_i] * ric[0];
            leftPose.leftCols<3>() = R0.transpose();
            leftPose.rightCols<1>() = -R0.transpose() * t0;
            //cout << "left pose " << leftPose << endl;

            Eigen::Matrix<double, 3, 4> rightPose;
            Eigen::Vector3d t1 = Ps[imu_i] + Rs[imu_i] * tic[1];
            Eigen::Matrix3d R1 = Rs[imu_i] * ric[1];
            rightPose.leftCols<3>() = R1.transpose();
            rightPose.rightCols<1>() = -R1.transpose() * t1;
            //cout << "right pose " << rightPose << endl;

            Eigen::Vector2d point0, point1;
            Eigen::Vector3d point3d;
            point0 = it_per_id.feature_per_frame[0].point.head(2);
            point1 = it_per_id.feature_per_frame[0].pointRight.head(2);
            //cout << "point0 " << point0.transpose() << endl;
            //cout << "point1 " << point1.transpose() << endl;

            triangulatePoint(leftPose, rightPose, point0, point1, point3d);
            Eigen::Vector3d localPoint;
            localPoint = leftPose.leftCols<3>() * point3d + leftPose.rightCols<1>();
            double depth = localPoint.z();
            if (depth > 0)
                it_per_id.estimated_depth = depth;
            else
                it_per_id.estimated_depth = INIT_DEPTH;
            /*
            Vector3d ptsGt = pts_gt[it_per_id.feature_id];
            printf("stereo %d pts: %f %f %f gt: %f %f %f \n",it_per_id.feature_id, point3d.x(), point3d.y(), point3d.z(),
                                                            ptsGt.x(), ptsGt.y(), ptsGt.z());
            */
            continue;
        }
        else if(it_per_id.feature_per_frame.size() > 1)
        {
            int imu_i = it_per_id.start_frame;
            Eigen::Matrix<double, 3, 4> leftPose;
            Eigen::Vector3d t0 = Ps[imu_i] + Rs[imu_i] * tic[0];
            Eigen::Matrix3d R0 = Rs[imu_i] * ric[0];
            leftPose.leftCols<3>() = R0.transpose();
            leftPose.rightCols<1>() = -R0.transpose() * t0;

            imu_i++;
            Eigen::Matrix<double, 3, 4> rightPose;
            Eigen::Vector3d t1 = Ps[imu_i] + Rs[imu_i] * tic[0];
            Eigen::Matrix3d R1 = Rs[imu_i] * ric[0];
            rightPose.leftCols<3>() = R1.transpose();
            rightPose.rightCols<1>() = -R1.transpose() * t1;

            Eigen::Vector2d point0, point1;
            Eigen::Vector3d point3d;
            point0 = it_per_id.feature_per_frame[0].point.head(2);
            point1 = it_per_id.feature_per_frame[1].point.head(2);
            triangulatePoint(leftPose, rightPose, point0, point1, point3d);
            Eigen::Vector3d localPoint;
            localPoint = leftPose.leftCols<3>() * point3d + leftPose.rightCols<1>();
            double depth = localPoint.z();
            if (depth > 0)
                it_per_id.estimated_depth = depth;
            else
                it_per_id.estimated_depth = INIT_DEPTH;
            /*
            Vector3d ptsGt = pts_gt[it_per_id.feature_id];
            printf("motion  %d pts: %f %f %f gt: %f %f %f \n",it_per_id.feature_id, point3d.x(), point3d.y(), point3d.z(),
                                                            ptsGt.x(), ptsGt.y(), ptsGt.z());
            */
            continue;
        }
        it_per_id.used_num = it_per_id.feature_per_frame.size();
        if (it_per_id.used_num < 4)
            continue;

        int imu_i = it_per_id.start_frame, imu_j = imu_i - 1;

        Eigen::MatrixXd svd_A(2 * it_per_id.feature_per_frame.size(), 4);
        int svd_idx = 0;

        Eigen::Matrix<double, 3, 4> P0;
        Eigen::Vector3d t0 = Ps[imu_i] + Rs[imu_i] * tic[0];
        Eigen::Matrix3d R0 = Rs[imu_i] * ric[0];
        P0.leftCols<3>() = Eigen::Matrix3d::Identity();
        P0.rightCols<1>() = Eigen::Vector3d::Zero();

        for (auto &it_per_frame : it_per_id.feature_per_frame)
        {
            imu_j++;

            Eigen::Vector3d t1 = Ps[imu_j] + Rs[imu_j] * tic[0];
            Eigen::Matrix3d R1 = Rs[imu_j] * ric[0];
            Eigen::Vector3d t = R0.transpose() * (t1 - t0);
            Eigen::Matrix3d R = R0.transpose() * R1;
            Eigen::Matrix<double, 3, 4> P;
            P.leftCols<3>() = R.transpose();
            P.rightCols<1>() = -R.transpose() * t;
            Eigen::Vector3d f = it_per_frame.point.normalized();
            svd_A.row(svd_idx++) = f[0] * P.row(2) - f[2] * P.row(0);
            svd_A.row(svd_idx++) = f[1] * P.row(2) - f[2] * P.row(1);

            if (imu_i == imu_j)
                continue;
        }
        ROS_ASSERT(svd_idx == svd_A.rows());
        Eigen::Vector4d svd_V = Eigen::JacobiSVD<Eigen::MatrixXd>(svd_A, Eigen::ComputeThinV).matrixV().rightCols<1>();
        double svd_method = svd_V[2] / svd_V[3];
        //it_per_id->estimated_depth = -b / A;
        //it_per_id->estimated_depth = svd_V[2] / svd_V[3];

        it_per_id.estimated_depth = svd_method;
        //it_per_id->estimated_depth = INIT_DEPTH;

        if (it_per_id.estimated_depth < 0.1)
        {
            it_per_id.estimated_depth = INIT_DEPTH;
        }

    }
}

void FeatureManager::removeOutlier(set<int> &outlierIndex)
{
    std::set<int>::iterator itSet;
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;
        int index = it->feature_id;
        itSet = outlierIndex.find(index);
        if(itSet != outlierIndex.end())
        {
            feature.erase(it);
            //printf("remove outlier %d \n", index);
        }
    }
}

void FeatureManager::removeBackShiftDepth(Eigen::Matrix3d marg_R, Eigen::Vector3d marg_P, Eigen::Matrix3d new_R, Eigen::Vector3d new_P)
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame != 0)
            it->start_frame--;
        else
        {
            Eigen::Vector3d uv_i = it->feature_per_frame[0].point;
            it->feature_per_frame.erase(it->feature_per_frame.begin());
            if (it->feature_per_frame.size() < 2)
            {
                feature.erase(it);
                continue;
            }
            else
            {
                Eigen::Vector3d pts_i = uv_i * it->estimated_depth;
                Eigen::Vector3d w_pts_i = marg_R * pts_i + marg_P;
                Eigen::Vector3d pts_j = new_R.transpose() * (w_pts_i - new_P);
                double dep_j = pts_j(2);
                if (dep_j > 0)
                    it->estimated_depth = dep_j;
                else
                    it->estimated_depth = INIT_DEPTH;
            }
        }
        // remove tracking-lost feature after marginalize
        /*
        if (it->endFrame() < WINDOW_SIZE - 1)
        {
            feature.erase(it);
        }
        */
    }
}

void FeatureManager::removeBack()
{
    for (auto it = feature.begin(), it_next = feature.begin();
         it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame != 0)
            it->start_frame--;
        else
        {
            it->feature_per_frame.erase(it->feature_per_frame.begin());
            if (it->feature_per_frame.size() == 0)
                feature.erase(it);
        }
    }
}

void FeatureManager::removeFront(int frame_count)
{
    for (auto it = feature.begin(), it_next = feature.begin(); it != feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame == frame_count)
        {
            it->start_frame--;
        }
        else
        {
            int j = WINDOW_SIZE - 1 - it->start_frame;
            if (it->endFrame() < frame_count - 1)
                continue;
            it->feature_per_frame.erase(it->feature_per_frame.begin() + j);
            if (it->feature_per_frame.size() == 0)
                feature.erase(it);
        }
    }
}

double FeatureManager::compensatedParallax2(const FeaturePerId &it_per_id, int frame_count)
{
    //check the second last frame is keyframe or not
    //parallax betwwen seconde last frame and third last frame
    const FeaturePerFrame &frame_i = it_per_id.feature_per_frame[frame_count - 2 - it_per_id.start_frame];
    const FeaturePerFrame &frame_j = it_per_id.feature_per_frame[frame_count - 1 - it_per_id.start_frame];

    double ans = 0;
    Vector3d p_j = frame_j.point;

    double u_j = p_j(0);
    double v_j = p_j(1);

    Vector3d p_i = frame_i.point;
    Vector3d p_i_comp;

    //int r_i = frame_count - 2;
    //int r_j = frame_count - 1;
    //p_i_comp = ric[camera_id_j].transpose() * Rs[r_j].transpose() * Rs[r_i] * ric[camera_id_i] * p_i;
    p_i_comp = p_i;
    double dep_i = p_i(2);
    double u_i = p_i(0) / dep_i;
    double v_i = p_i(1) / dep_i;
    double du = u_i - u_j, dv = v_i - v_j;

    double dep_i_comp = p_i_comp(2);
    double u_i_comp = p_i_comp(0) / dep_i_comp;
    double v_i_comp = p_i_comp(1) / dep_i_comp;
    double du_comp = u_i_comp - u_j, dv_comp = v_i_comp - v_j;

    ans = max(ans, sqrt(min(du * du + dv * dv, du_comp * du_comp + dv_comp * dv_comp)));

    return ans;
}

void FeatureManager::triangulateLine(Vector3d Ps[], Matrix3d Rs[], Vector3d tic[], Matrix3d ric[])
{
    Vector4d left_plane, right_plane;
    Vector3d left_plane_3d, right_plane_3d;

    Vector3d direction_l;
    Vector3d normal_l;
    Eigen::Matrix<double, 3, 3, Eigen::RowMajor> Rotation_psi;

    Vector3d sp_3d;
    Vector3d ep_3d;

    int index = -1;
//    cvtColor(img, img, COLOR_GRAY2BGR);
    for(auto &it_per_id : line_feature)
    {
        it_per_id.used_num = it_per_id.line_feature_per_frame.size();
        if(it_per_id.is_triangulation == 1 || it_per_id.line_feature_per_frame.size() < 2)
            continue;

        int imu_i = it_per_id.start_frame;
        int imu_j = it_per_id.start_frame + it_per_id.used_num - 1;

        Matrix3d R_left = Rs[imu_i] * ric[0];
        Quaterniond q_left(R_left);
        Vector3d t_left = Rs[imu_i] * tic[0] + Ps[imu_i] ;

        Matrix3d R_right = Rs[imu_j] * ric[0];
        Quaterniond q_right(R_right);
        Vector3d t_right = Rs[imu_j] * tic[0] + Ps[imu_j] ;

        Quaterniond relative_q = q_left.inverse() * q_right;
        Vector3d relative_t = q_left.inverse().toRotationMatrix() * (t_right - t_left);

        Vector3d left_sp = it_per_id.line_feature_per_frame[0].start_point;
        Vector3d left_ep = it_per_id.line_feature_per_frame[0].end_point;

        Vector3d right_sp = it_per_id.line_feature_per_frame[it_per_id.used_num - 1].start_point;
        Vector3d right_ep = it_per_id.line_feature_per_frame[it_per_id.used_num - 1].end_point;

        Vector3d right_sp_l = relative_q * right_sp;
        Vector3d right_ep_l = relative_q * right_ep;

        Vector3d t_cj_ci = q_right.inverse().toRotationMatrix() * (t_left - t_right);
        t_cj_ci = t_cj_ci/t_cj_ci(2);

//        if(it_per_id.solve_flag == 0)
//        {

        calcPluckerLine(left_sp, left_ep, right_sp_l, right_ep_l,
                        Vector3d(0, 0, 0), relative_t, direction_l, normal_l,
                        left_plane, right_plane);

        Matrix4d L_c;
        L_c.setZero();
        L_c.block<3,3>(0,0) = Utility::skewSymmetric(normal_l);
        L_c.block<3,1>(0,3) = direction_l;
        L_c.block<1,3>(3,0) = -direction_l.transpose();

        double scale = 1.0;
        Vector3d sp_2d_c = it_per_id.line_feature_per_frame[0].start_point;
        Vector3d ep_2d_c = it_per_id.line_feature_per_frame[0].end_point;
        Vector3d sp_2d_p_c = Vector3d(sp_2d_c(0) + scale, -scale*(ep_2d_c(0) - sp_2d_c(0))/(ep_2d_c(1) - sp_2d_c(1)) + sp_2d_c(1), 1);
        Vector3d ep_2d_p_c = Vector3d(ep_2d_c(0) + scale, -scale*(ep_2d_c(0) - sp_2d_c(0))/(ep_2d_c(1) - sp_2d_c(1)) + ep_2d_c(1), 1);

        Vector3d pi_s = sp_2d_c.cross(sp_2d_p_c);
        Vector3d pi_e = ep_2d_c.cross(ep_2d_p_c);

        Vector4d pi_s_4d, pi_e_4d;
        pi_s_4d.head(3) = pi_s;
        pi_s_4d(3) = 0;
        pi_e_4d.head(3) = pi_e;
        pi_e_4d(3) = 0;

        Vector4d D_s = L_c * pi_s_4d;
        Vector4d D_e = L_c * pi_e_4d;
        Vector3d D_s_3d(D_s(0)/D_s(3), D_s(1)/D_s(3), D_s(2)/D_s(3));
        Vector3d D_e_3d(D_e(0)/D_e(3), D_e(1)/D_e(3), D_e(2)/D_e(3));

        if(D_s_3d(2) < 0 || D_e_3d(2) < 0 ||
                isnan(D_s_3d(0)) || isnan(D_s_3d(1)) || isnan(D_s_3d(2)) ||
                isnan(D_e_3d(0)) || isnan(D_e_3d(1)) || isnan(D_e_3d(2)))
            continue;

        Matrix<double, 6, 1> line_w, line_l;
        line_l.block<3,1>(0,0) = normal_l;
        line_l.block<3,1>(3,0) = direction_l;

        Matrix<double, 6, 6> T_wl;
        T_wl.setZero();
        T_wl.block<3,3>(0,0) = R_left;
        T_wl.block<3,3>(0,3) = Utility::skewSymmetric(t_left) * R_left;
        T_wl.block<3,3>(3,3) = R_left;

        line_w = T_wl * line_l;

        Vector3d n_w, d_w;
        n_w = line_w.block<3,1>(0,0);
        d_w = line_w.block<3,1>(3,0);

        Rotation_psi.block<3,1>(0,0) = n_w/(n_w.norm());
        Rotation_psi.block<3,1>(0,1) = d_w/(d_w.norm());
        Rotation_psi.block<3,1>(0,2) = n_w.cross(d_w)/(n_w.cross(d_w).norm());

//        if(n_w.norm()/d_w.norm() < 1.0)
//            continue;

        it_per_id.orthonormal_vec.head(3) = Rotation_psi.eulerAngles(0,1,2);
        it_per_id.orthonormal_vec(3) = atan2(d_w.norm(), n_w.norm());
        it_per_id.is_triangulation = 1;

        index++;
    }
}


void FeatureManager::calcPluckerLine(const Vector3d _prev_sp, const Vector3d _prev_ep,
                                     const Vector3d _curr_sp, const Vector3d _curr_ep,
                                     const Vector3d _origin_prev, const Vector3d _origin_curr,
                                     Vector3d &_out_direction_vec, Vector3d &_out_normal_vec,
                                     Vector4d &prev_plane, Vector4d &curr_plane)
{
    //  if ( good_match_vector.size() == 0 )
    //    return;
    //  //for(int i = 0; i < 1; i++) //_good_match_vector.size(); i++)
    //  int i = 0;

    //  Point2f prev_srt_pts = _prev_keyLine.at(_good_match_vector.at(i).queryIdx).getStartPoint();
    //  Point2f prev_end_pts = _prev_keyLine.at(_good_match_vector.at(i).queryIdx).getEndPoint();

    //  Point2f curr_srt_pts = _curr_keyLine.at(_good_match_vector.at(i).trainIdx).getStartPoint();
    //  Point2f curr_end_pts = _curr_keyLine.at(_good_match_vector.at(i).trainIdx).getEndPoint();


    Matrix3d prev_srt_skew;
    skewMatFromVector3d(_prev_sp, prev_srt_skew);
    Vector3d prev_end_vec = _prev_ep;
    Vector3d prev_mul_skew_vec = prev_srt_skew * prev_end_vec;
//    prev_mul_skew_vec.normalize();
    //    Vector4d prev_plane;
    prev_plane(0) = prev_mul_skew_vec(0);
    prev_plane(1) = prev_mul_skew_vec(1);
    prev_plane(2) = prev_mul_skew_vec(2);
    prev_plane(3) =
            prev_mul_skew_vec(0) * _origin_prev(0) +
            prev_mul_skew_vec(1) * _origin_prev(1) +
            prev_mul_skew_vec(2) * _origin_prev(2);

    Matrix3d curr_srt_skew;
    skewMatFromVector3d(_curr_sp, curr_srt_skew);
    //    Vector3d curr_end_vec = Vector3d(_curr_ep[0], _curr_ep[1], 1.0);
    Vector3d curr_end_vec = _curr_ep;
    Vector3d curr_mul_skew_vec = curr_srt_skew * curr_end_vec;
//    curr_mul_skew_vec.normalize();
    //    Vector4d curr_plane;
    curr_plane(0) = curr_mul_skew_vec(0);
    curr_plane(1) = curr_mul_skew_vec(1);
    curr_plane(2) = curr_mul_skew_vec(2);
    curr_plane(3) =
            curr_mul_skew_vec(0) * _origin_curr(0) +
            curr_mul_skew_vec(1) * _origin_curr(1) +
            curr_mul_skew_vec(2) * _origin_curr(2);

    prev_plane(3) = - prev_plane(3);
    curr_plane(3) = - curr_plane(3);
    /*
        cout <<"IN CALCPLUCK" <<endl;
        cout << prev_plane << endl;
        cout << curr_plane << endl;
    */
    //  cout <<"IN linefeaturetracker calcPlucker" << endl;
    //  cout <<"prev_srt skew: "<< curr_srt_skew << endl;
    //  cout <<"prev_end_vec: "<< curr_end_vec << endl;
    //  cout <<"prev_plane: "<< curr_plane << endl;
    //  cout <<"prev_plane^T: "<< curr_plane.transpose() << endl;


    Matrix4d dualPluckerMat = prev_plane*curr_plane.transpose() - curr_plane * prev_plane.transpose();
    /*
  cout <<"prev_plane*curr_plane.transpose(): "<< prev_plane*curr_plane.transpose() << endl;
  cout <<"curr_plane * prev_plane.transpose(): "<< curr_plane * prev_plane.transpose() << endl;
  cout <<"dualPluckerMat: "<< dualPluckerMat << endl;
  */

    _out_direction_vec(0) = dualPluckerMat(2,1);
    _out_direction_vec(1) = dualPluckerMat(0,2);
    _out_direction_vec(2) = dualPluckerMat(1,0);

    _out_normal_vec(0) = dualPluckerMat(0,3);
    _out_normal_vec(1) = dualPluckerMat(1,3);
    _out_normal_vec(2) = dualPluckerMat(2,3);
}

void FeatureManager::skewMatFromVector3d(const Vector3d &_in_pt, Matrix3d &_out_skew_mat)
{
    // INPUT
    //w1 w2 w3
    // OUTPUT
    // 0 -w3 w2
    // w3 0 -w1
    // -w2 w1 0
    _out_skew_mat(0,0) = 0.0;
    _out_skew_mat(0,1) = -1.0 * _in_pt[2];
    _out_skew_mat(0,2) = 1.0 * _in_pt[1];

    _out_skew_mat(1,0) = 1.0 * _in_pt[2];
    _out_skew_mat(1,1) = 0.0;
    _out_skew_mat(1,2) = -1.0 * _in_pt[0];

    _out_skew_mat(2,0) = -1.0 * _in_pt[1];
    _out_skew_mat(2,1) = 1.0 * _in_pt[0];
    _out_skew_mat(2,2) = 0.0;

}

void FeatureManager::removeLineBack()
{
    for (auto it = line_feature.begin(), it_next = line_feature.begin();
         it != line_feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame != 0)
            it->start_frame--;
        else
        {
            it->line_feature_per_frame.erase(it->line_feature_per_frame.begin());
            if (it->line_feature_per_frame.size() == 0)
                line_feature.erase(it);
        }
    }
}

void FeatureManager::removeLineFront(int frame_count)
{
    for (auto it = line_feature.begin(), it_next = line_feature.begin(); it != line_feature.end(); it = it_next)
    {
        it_next++;

        if (it->start_frame == frame_count)
        {
            it->start_frame--;
        }
        else
        {
            int j = WINDOW_SIZE - 1 - it->start_frame;
            if (it->endFrame() < frame_count - 1)
                continue;
            it->line_feature_per_frame.erase(it->line_feature_per_frame.begin() + j);
            if (it->line_feature_per_frame.size() == 0)
                line_feature.erase(it);
        }
    }
}

void FeatureManager::removeLineFailures()
{
    for(auto it = line_feature.begin(), it_next = line_feature.begin();
        it != line_feature.end(); it = it_next)
    {
        it_next++;
        if(it->solve_flag == 2)
            line_feature.erase(it);
    }
}

vector<Vector4d> FeatureManager::getLineOrthonormal()
{
    vector<Vector4d> LineOrtho(getLineFeatureCount());
    int line_feature_index =-1;
    for(auto &it_line_per_id : line_feature)
    {
        it_line_per_id.used_num = it_line_per_id.line_feature_per_frame.size();
        if(it_line_per_id.used_num < LINE_WINDOW || it_line_per_id.is_triangulation == 0)
            continue;
//        if(it_line_per_id.solve_flag == 0)
//            continue;

        Vector4d tmp_vector;
        tmp_vector = it_line_per_id.orthonormal_vec;
        LineOrtho[++line_feature_index] = tmp_vector; //it_line_per_id.orthonormal_vec;

//        cout << tmp_vector[0] << ", " <<
//                tmp_vector[1] << ", " <<
//                tmp_vector[2] << ", " <<
//                tmp_vector[3] << endl;
    }
    return LineOrtho;
}

void FeatureManager::setLineOrtho(vector<Vector4d> &get_lineOrtho, Vector3d Ps[], Matrix3d Rs[],Vector3d tic[], Matrix3d ric[])
{
    int line_feature_index =-1;
    for (auto &it_per_id : line_feature)
    {
        it_per_id.used_num = it_per_id.line_feature_per_frame.size();
        if(it_per_id.used_num < LINE_WINDOW || it_per_id.is_triangulation == 0)
            continue;
//        if(it_line_per_id.solve_flag == 0)
//            continue;

        ++line_feature_index;

//        cout << "-------" << endl;
//        cout << it_line_per_id.orthonormal_vec[0] << " -> " << get_lineOrtho.at(line_feature_index)[0] << endl;
//        cout << it_line_per_id.orthonormal_vec[1] << " -> " << get_lineOrtho.at(line_feature_index)[1] << endl;
//        cout << it_line_per_id.orthonormal_vec[2] << " -> " << get_lineOrtho.at(line_feature_index)[2] << endl;
//        cout << it_line_per_id.orthonormal_vec[3] << " -> " << get_lineOrtho.at(line_feature_index)[3] << endl;

        Vector3d sp_3d_c, ep_3d_c, sp_3d_w, ep_3d_w;
        ortho2ep(it_per_id, Ps, Rs, tic, ric,
                 sp_3d_c, ep_3d_c, sp_3d_w, ep_3d_w);

        Vector3d sp_est = sp_3d_c/sp_3d_c(2);
        Vector3d ep_est = ep_3d_c/ep_3d_c(2);

        Vector3d sp_obs = it_per_id.line_feature_per_frame.back().start_point;
        Vector3d ep_obs = it_per_id.line_feature_per_frame.back().end_point;

        if(sp_3d_c(2) < 0 || ep_3d_c(2) < 0
                || (sp_est - sp_obs).norm() > 0.1 || (ep_est - ep_obs).norm() > 0.1)
        {
            it_per_id.solve_flag = 2;
            continue;
        }
        else
            it_per_id.solve_flag = 1;

        it_per_id.orthonormal_vec[0] = get_lineOrtho.at(line_feature_index)[0];
        it_per_id.orthonormal_vec[1] = get_lineOrtho.at(line_feature_index)[1];
        it_per_id.orthonormal_vec[2] = get_lineOrtho.at(line_feature_index)[2];
        it_per_id.orthonormal_vec[3] = get_lineOrtho.at(line_feature_index)[3];
    }
}


int FeatureManager::getLineFeatureCount()
{
    int cnt = 0;
    for (auto &it : line_feature)
    {
        it.used_num = it.line_feature_per_frame.size();

        //std::cout << "LINE/frame #: " << it.line_feature_per_frame.size() << std::endl;
        if(it.used_num < LINE_WINDOW || it.is_triangulation == 0)
            continue;
//        if(it.solve_flag == 0)
//            continue;

        cnt++;

    }
    return cnt;
}

void FeatureManager::ortho2ep(LineFeaturePerId linefeature,
                              Vector3d Ps[], Matrix3d Rs[], Vector3d tic[], Matrix3d ric[],
                              Vector3d &sp_3d_c, Vector3d &ep_3d_c, Vector3d &sp_3d_w, Vector3d &ep_3d_w)
{
    int scale = 1.0;
    int used_num = linefeature.line_feature_per_frame.size();
    int imu_i = linefeature.start_frame;
    int imu_j = linefeature.start_frame + used_num - 1;

    Matrix3d R_wc = Rs[imu_j] * ric[0];
    Vector3d t_wc = Rs[imu_j] * tic[0] + Ps[imu_j] ;

    Vector3d sp_c_2d = linefeature.line_feature_per_frame[used_num-1].start_point;
    Vector3d ep_c_2d = linefeature.line_feature_per_frame[used_num-1].end_point;

    Vector3d sp_p_c = Vector3d(sp_c_2d(0) + scale, -scale*(ep_c_2d(0) - sp_c_2d(0))/(ep_c_2d(1) - sp_c_2d(1)) + sp_c_2d(1), 1);
    Vector3d ep_p_c = Vector3d(ep_c_2d(0) + scale, -scale*(ep_c_2d(0) - sp_c_2d(0))/(ep_c_2d(1) - sp_c_2d(1)) + ep_c_2d(1), 1);

    Vector3d pi_s = sp_c_2d.cross(sp_p_c);
    Vector3d pi_e = ep_c_2d.cross(ep_p_c);

    Vector4d pi_s_4d, pi_e_4d;
    pi_s_4d.head(3) = pi_s;
    pi_s_4d(3) = 0;
    pi_e_4d.head(3) = pi_e;
    pi_e_4d(3) = 0;

    AngleAxisd roll(linefeature.orthonormal_vec(0), Vector3d::UnitX());
    AngleAxisd pitch(linefeature.orthonormal_vec(1), Vector3d::UnitY());
    AngleAxisd yaw(linefeature.orthonormal_vec(2), Vector3d::UnitZ());
    Eigen::Matrix<double, 3, 3, Eigen::RowMajor> Rotation_psi;
    Rotation_psi = roll * pitch * yaw;
    double pi = linefeature.orthonormal_vec(3);

    Vector3d n_w = cos(pi) * Rotation_psi.block<3,1>(0,0);
    Vector3d d_w = sin(pi) * Rotation_psi.block<3,1>(0,1);

    Matrix<double, 6, 1> line_w;
    line_w.block<3,1>(0,0) = n_w;
    line_w.block<3,1>(3,0) = d_w;

    Matrix<double, 6, 6> T_cw;
    T_cw.setZero();
    T_cw.block<3,3>(0,0) = R_wc.transpose();
    T_cw.block<3,3>(0,3) = Utility::skewSymmetric(-R_wc.transpose()*t_wc) * R_wc.transpose();
    T_cw.block<3,3>(3,3) = R_wc.transpose();

    Matrix<double, 6, 1> line_c = T_cw * line_w;
    Vector3d n_c = line_c.block<3,1>(0,0);
    Vector3d d_c = line_c.block<3,1>(3,0);

    Matrix4d L_c;
    L_c.setZero();
    L_c.block<3,3>(0,0) = Utility::skewSymmetric(n_c);
    L_c.block<3,1>(0,3) = d_c;
    L_c.block<1,3>(3,0) = -d_c.transpose();

    Vector4d D_s = L_c * pi_s_4d;
    Vector4d D_e = L_c * pi_e_4d;
    sp_3d_c = Vector3d(D_s(0)/D_s(3), D_s(1)/D_s(3), D_s(2)/D_s(3));
    ep_3d_c = Vector3d(D_e(0)/D_e(3), D_e(1)/D_e(3), D_e(2)/D_e(3));

    sp_3d_w = R_wc * sp_3d_c + t_wc;
    ep_3d_w = R_wc * ep_3d_c + t_wc;
}

