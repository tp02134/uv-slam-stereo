#pragma once

#include <cstdio>
#include <iostream>
#include <queue>
#include <execinfo.h>
#include <csignal>
#include <random>
#include <fstream>


#include <opencv2/opencv.hpp>
#include <eigen3/Eigen/Dense>

#include <boost/algorithm/string.hpp>


#include "camodocal/camera_models/CameraFactory.h"
#include "camodocal/camera_models/CataCamera.h"
#include "camodocal/camera_models/PinholeCamera.h"
#include "camodocal/camera_models/EquidistantCamera.h"

#include "../estimator/parameters.h"
#include "../utility/tic_toc.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/line_descriptor.hpp>
#include <opencv2/line_descriptor/descriptor.hpp>
#include <opencv/cv.h>

#include "math.h"
#include "../utility/utility.h"
#include "highgui.h"
#include "ELSED.h"

using namespace std;
using namespace camodocal;
using namespace Eigen;
using namespace cv;
using namespace line_descriptor;

typedef line_descriptor::BinaryDescriptor LineBD;
typedef line_descriptor::KeyLine LineKL;

class LineFeatureTracker
{
  public:
    LineFeatureTracker();

    map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> trackImage(double _cur_time, const cv::Mat &_img, const cv::Mat &_img1);
    map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> trackImage(double _cur_time, const cv::Mat &_img);
    void imageUndistortion(camodocal::CameraPtr cam,Mat &_img, Mat &_out_undistort_img);
//    void readIntrinsicParameter(const string &calib_file);
    void lineExtraction( Mat &cur_img, vector<LineKL> &_keyLine, Mat &_descriptor );
    void lineMatching( vector<LineKL> &_prev_keyLine, vector<LineKL> &_curr_keyLine, Mat &_prev_descriptor, Mat &_curr_descriptor, vector<DMatch> &_good_match_vector);
    void normalizePoints();

    void getVPHypVia2Lines(vector<KeyLine> cur_keyLine, vector<Vector3d> &para_vector, vector<double> &length_vector, vector<double> &orientation_vector,
                           std::vector<std::vector<Vector3d> > &vpHypo );
    void getSphereGrids(vector<KeyLine> cur_keyLine, vector<Vector3d> &para_vector, vector<double> &length_vector, vector<double> &orientation_vector,
                        std::vector<std::vector<double> > &sphereGrid );
    void getBestVpsHyp( std::vector<std::vector<double> > &sphereGrid, std::vector<std::vector<Vector3d> >  &vpHypo, std::vector<Vector3d> &vps  );
    void lines2Vps(vector<KeyLine> cur_keyLine, double thAngle, std::vector<Vector3d> &vps, std::vector<std::vector<int> > &clusters, vector<int> &vp_idx);
    void drawClusters( cv::Mat &img, std::vector<KeyLine> &lines, std::vector<std::vector<int> > &clusters );

    void lineRawResolution( Mat &cur_img, vector<LineKL> &predict_keyLines);
    void updateID();

    vector<camodocal::CameraPtr> m_camera;
    string model_type;
    camodocal::EquidistantCameraPtr equidistant_camera;
    camodocal::PinholeCameraPtr pinhole_camera;

    Mat new_camera_matrix, undist_map1, undist_map2;

    Mat prev_img, cur_img;
    Mat prev_descriptor, cur_descriptor, m_matched_descriptor;

    vector<Point2f> prev_start_pts, prev_end_pts;
    vector<Point2f> cur_start_pts, cur_end_pts;
    vector<Point2f> cur_start_un_pts, cur_end_un_pts;
    vector<line_descriptor::KeyLine> prev_keyLine, cur_keyLine, m_matched_keyLines;

    vector<int> ids, tmp_ids; // set every value of tmp_ids to -1 when the new lines are extracted
    vector<int> track_cnt, tmp_track_cnt; // not sure // initial value: 1
    vector<int> vp_ids, tmp_vp_ids;
    vector<Vector3d> vps;
    vector<Point2f> start_pts_velocity, end_pts_velocity;
    vector<Point2f> prev_start_un_pts, curr_start_un_pts, prev_end_un_pts, curr_end_un_pts; // not sure
    Vector3d vp;


    static int n_id;
    static int vp_id;
    int image_id = 0;

    Utility util;


    /// FOR KALMAN
    // States are position and velocity in X and Y directions; four states [X;Y;dX/dt;dY/dt]
    CvPoint pt_Prediction, pt_Correction;

    // Measurements are current position of the mouse [X;Y]
    CvMat* measurement = cvCreateMat(2, 1, CV_32FC1);

    // dynamic params (4), measurement params (2), control params (0)
//    CvKalman* kalman  = cvCreateKalman(4, 2, 0);

    void CannyDetection(Mat &src, vector<line_descriptor::KeyLine> &keylines);
    bool getPointChain( const Mat & img, const Point2f pt, Point2f * chained_pt, int & direction, int step );
    void extractSegments( vector<Point2f> * points, vector<line_descriptor::KeyLine> * keylines);
    double distPointLine( const Mat & p, Mat & l );
    void additionalOperationsOnSegments(Mat & src, line_descriptor::KeyLine * kl);
    void pointInboardTest(Mat & src, Point2f * pt);
    bool mergeSegments(line_descriptor::KeyLine * kl1, line_descriptor::KeyLine * kl2, line_descriptor::KeyLine * kl_merged );
    void mergeLines(line_descriptor::KeyLine * kl1, line_descriptor::KeyLine * kl2, line_descriptor::KeyLine * kl_merged);
    void HoughDetection(Mat &src, vector<line_descriptor::KeyLine> &keylines);

    double Union_dist(VectorXd a, VectorXd b);
    double Intersection_dist(VectorXd a, VectorXd b);
    VectorXd Union(VectorXd a, VectorXd b);
    VectorXd Intersection(VectorXd a, VectorXd b);
    double SafeAcos (double x);
    void removeRow(Eigen::MatrixXd& matrix, unsigned int rowToRemove) ;
    void removeColumn(Eigen::MatrixXd& matrix, unsigned int colToRemove);

    void reduceVector(vector<Point2f> &v, vector<uchar> status);
    void readIntrinsicParameter(const vector<string> &calib_file);
    bool stereo_cam;
    void drawTrack(const Mat img, vector<int> ids, vector<Point2f> start_pts, vector<Point2f> end_pts);
    cv::Mat getTrackImage();
    template<class tType>
    void incidentPoint( tType * pt, Mat & l );

    int threshold_length = 20;
    double threshold_dist = 1.5;
    int imagewidth, imageheight;
    int ROW_MARGIN = 15;
    int COL_MARGIN = 20;

    cv::Mat imTrack;

};
