#include "line_feature_tracker.h"

#include <chrono>

int LineFeatureTracker::n_id = 0;
int LineFeatureTracker::vp_id = 0;
unsigned int frame_count = 0;

///// TMP OUT FOR DEBUG
unsigned int matched_count = 0;
int keyLine_id = 0;
int img_num = 0;

upm::ELSED elsed;

LineFeatureTracker::LineFeatureTracker()
{
    stereo_cam = 0;
}

map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> LineFeatureTracker::trackImage(double _cur_time, const cv::Mat &_img, const cv::Mat &_img1)
{
    cur_img = _img.clone();
    cv::Mat right_img = _img1;
    Mat un_cur_img, un_cur_right_img;

//    TicToc t_r;
    imageUndistortion(m_camera[0], cur_img, un_cur_img);
//    imageUndistortion(m_camera[1], right_img, un_cur_right_img);
//    cout << t_r.toc() << endl;

    vector<Vector3d> para_vector;
    vector<double> length_vector;
    vector<double> orientation_vector;
    vector<std::vector<Vector3d> > vpHypo;
    vector<std::vector<double> > sphereGrid;
    vector<Vector3d> tmp_vps;
    vector<vector<int>> clusters;
    vector<int> local_vp_ids;
    double thAngle = 1.0 / 180.0 * CV_PI;

    cur_keyLine.clear();
    cur_start_pts.clear();
    cur_end_pts.clear();

    tmp_track_cnt.clear();
    tmp_ids.clear();
    tmp_vp_ids.clear();
    start_pts_velocity.clear();
    end_pts_velocity.clear();
    vps.clear();

    lineExtraction(un_cur_img, cur_keyLine, cur_descriptor);
    imshow("1", un_cur_img);
    waitKey(1);

    if(cur_keyLine.size() > 2)
    {
        getVPHypVia2Lines(cur_keyLine, para_vector, length_vector, orientation_vector, vpHypo);
        getSphereGrids(cur_keyLine, para_vector, length_vector, orientation_vector, sphereGrid );
        getBestVpsHyp(sphereGrid, vpHypo, tmp_vps);
        lines2Vps(cur_keyLine, thAngle, tmp_vps, clusters, local_vp_ids);
    }

    for(int i = 0; i < cur_keyLine.size(); i++)
    {
        cv::Point2f start_pts = cur_keyLine[i].getStartPoint();
        cv::Point2f end_pts = cur_keyLine[i].getEndPoint();
        cur_start_pts.push_back( start_pts );
        cur_end_pts.push_back(end_pts);

        tmp_track_cnt.push_back(1);
        tmp_ids.push_back(-1);
        start_pts_velocity.push_back({0, 0});
        end_pts_velocity.push_back({0, 0});

        if(cur_keyLine.size() > 2)
        {
            if(!local_vp_ids.empty())
            {
                if(local_vp_ids[i] == 3)
                    vps.push_back(Vector3d(0.0,0.0,0.0));
                else
                    vps.push_back(tmp_vps[local_vp_ids[i]]/tmp_vps[local_vp_ids[i]](2));
            }
        }
        else
        {
            vps.push_back(Vector3d(0.0,0.0,0.0));
        }
    }

    if (prev_keyLine.size() > 0 && cur_keyLine.size() > 0)
    {
        vector<DMatch> good_match_vector;
        lineMatching(prev_keyLine, cur_keyLine, prev_descriptor, cur_descriptor, good_match_vector);
        Mat match_img;
        drawLineMatches(prev_img, prev_keyLine, cur_img, cur_keyLine, good_match_vector, match_img);

        if(good_match_vector.size() > 0)
            for(int i=0; i< good_match_vector.size(); i++)
            {
                tmp_ids.at(good_match_vector.at(i).trainIdx)=ids.at(good_match_vector.at(i).queryIdx);
                tmp_track_cnt.at(good_match_vector.at(i).trainIdx)=track_cnt.at(good_match_vector.at(i).queryIdx)+1;
            }
    }

    ids.clear();
    track_cnt.clear();
    ids = tmp_ids;
    track_cnt = tmp_track_cnt;

    updateID();
    normalizePoints();

    prev_img = cur_img.clone();
    prev_start_pts = cur_start_pts;
    prev_end_pts = cur_end_pts;
    prev_keyLine = cur_keyLine;
    prev_descriptor = cur_descriptor.clone();

    if(SHOW_TRACK)
        drawTrack(un_cur_img, ids, cur_start_pts, cur_end_pts);

    map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> linefeatureFrame;
    for (size_t i = 0; i < ids.size(); i++)
    {
        int feature_id = ids[i];
        int camera_id = 0;
        double s_x, s_y, e_x, e_y;
        s_x = cur_start_un_pts[i].x;
        s_y = cur_start_un_pts[i].y;
        e_x = cur_end_un_pts[i].x;
        e_y = cur_end_un_pts[i].y;
        double s_u, s_v, e_u, e_v;
        s_u = cur_start_pts[i].x;
        s_v = cur_start_pts[i].y;
        e_u = cur_end_pts[i].x;
        e_v = cur_end_pts[i].y;
        double sv_x, sv_y, ev_x, ev_y;
        sv_x = start_pts_velocity[i].x;
        sv_y = start_pts_velocity[i].y;
        ev_x = end_pts_velocity[i].x;
        ev_y = end_pts_velocity[i].y;
        double v_x, v_y, v_z;
        v_x = vps[i](0);
        v_y = vps[i](1);
        v_z = vps[i](2);

        Eigen::Matrix<double, 15, 1> line;
        line << s_x, s_y, e_x, e_y,
                s_u, s_v, e_u, e_v,
                sv_x, sv_y, ev_x, ev_y,
                v_x, v_y, v_z;
        linefeatureFrame[feature_id].emplace_back(camera_id, line);
//        cout << feature_id << endl;
    }

//    cout << ids.size() << ", " << linefeatureFrame.size() << endl;
    return linefeatureFrame;
}

map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> LineFeatureTracker::trackImage(double _cur_time, const cv::Mat &_img)
{
    cur_img = _img.clone();
//    cv::Mat right_img = _img1;
    Mat un_cur_img, un_cur_right_img;

//    TicToc t_r;
    imageUndistortion(m_camera[0], cur_img, un_cur_img);
//    imageUndistortion(m_camera[1], right_img, un_cur_right_img);
//    cout << t_r.toc() << endl;

    vector<Vector3d> para_vector;
    vector<double> length_vector;
    vector<double> orientation_vector;
    vector<std::vector<Vector3d> > vpHypo;
    vector<std::vector<double> > sphereGrid;
    vector<Vector3d> tmp_vps;
    vector<vector<int>> clusters;
    vector<int> local_vp_ids;
    double thAngle = 1.0 / 180.0 * CV_PI;

    cur_keyLine.clear();
    cur_start_pts.clear();
    cur_end_pts.clear();

    tmp_track_cnt.clear();
    tmp_ids.clear();
    tmp_vp_ids.clear();
    start_pts_velocity.clear();
    end_pts_velocity.clear();
    vps.clear();

    lineExtraction(un_cur_img, cur_keyLine, cur_descriptor);

    if(cur_keyLine.size() > 2)
    {
        getVPHypVia2Lines(cur_keyLine, para_vector, length_vector, orientation_vector, vpHypo);
        getSphereGrids(cur_keyLine, para_vector, length_vector, orientation_vector, sphereGrid );
        getBestVpsHyp(sphereGrid, vpHypo, tmp_vps);
        lines2Vps(cur_keyLine, thAngle, tmp_vps, clusters, local_vp_ids);
    }

    for(int i = 0; i < cur_keyLine.size(); i++)
    {
        cv::Point2f start_pts = cur_keyLine[i].getStartPoint();
        cv::Point2f end_pts = cur_keyLine[i].getEndPoint();
        cur_start_pts.push_back( start_pts );
        cur_end_pts.push_back(end_pts);

        tmp_track_cnt.push_back(1);
        tmp_ids.push_back(-1);
        start_pts_velocity.push_back({0, 0});
        end_pts_velocity.push_back({0, 0});

        if(cur_keyLine.size() > 2)
        {
            if(!local_vp_ids.empty())
            {
                if(local_vp_ids[i] == 3)
                    vps.push_back(Vector3d(0.0,0.0,0.0));
                else
                    vps.push_back(tmp_vps[local_vp_ids[i]]/tmp_vps[local_vp_ids[i]](2));
            }
        }
        else
        {
            vps.push_back(Vector3d(0.0,0.0,0.0));
        }
    }

    if (prev_keyLine.size() > 0 && cur_keyLine.size() > 0)
    {
        vector<DMatch> good_match_vector;
        lineMatching(prev_keyLine, cur_keyLine, prev_descriptor, cur_descriptor, good_match_vector);
//        Mat match_img;
//        drawLineMatches(prev_img, prev_keyLine, cur_img, cur_keyLine, good_match_vector, match_img);

        if(good_match_vector.size() > 0)
            for(int i=0; i< good_match_vector.size(); i++)
            {
                tmp_ids.at(good_match_vector.at(i).trainIdx)=ids.at(good_match_vector.at(i).queryIdx);
                tmp_track_cnt.at(good_match_vector.at(i).trainIdx)=track_cnt.at(good_match_vector.at(i).queryIdx)+1;
            }
    }

    ids.clear();
    track_cnt.clear();
    ids = tmp_ids;
    track_cnt = tmp_track_cnt;

    updateID();
    normalizePoints();

    prev_img = cur_img.clone();
    prev_start_pts = cur_start_pts;
    prev_end_pts = cur_end_pts;
    prev_keyLine = cur_keyLine;
    prev_descriptor = cur_descriptor.clone();

    if(SHOW_TRACK)
        drawTrack(un_cur_img, ids, cur_start_pts, cur_end_pts);

    map<int, vector<pair<int, Eigen::Matrix<double, 15, 1>>>> linefeatureFrame;
    for (size_t i = 0; i < ids.size(); i++)
    {
        int feature_id = ids[i];
        int camera_id = 0;
        double s_x, s_y, e_x, e_y;
        s_x = cur_start_un_pts[i].x;
        s_y = cur_start_un_pts[i].y;
        e_x = cur_end_un_pts[i].x;
        e_y = cur_end_un_pts[i].y;
        double s_u, s_v, e_u, e_v;
        s_u = cur_start_pts[i].x;
        s_v = cur_start_pts[i].y;
        e_u = cur_end_pts[i].x;
        e_v = cur_end_pts[i].y;
        double sv_x, sv_y, ev_x, ev_y;
        sv_x = start_pts_velocity[i].x;
        sv_y = start_pts_velocity[i].y;
        ev_x = end_pts_velocity[i].x;
        ev_y = end_pts_velocity[i].y;
        double v_x, v_y, v_z;
        v_x = vps[i](0);
        v_y = vps[i](1);
        v_z = vps[i](2);

        Eigen::Matrix<double, 15, 1> line;
        line << s_x, s_y, e_x, e_y,
                s_u, s_v, e_u, e_v,
                sv_x, sv_y, ev_x, ev_y,
                v_x, v_y, v_z;
        linefeatureFrame[feature_id].emplace_back(camera_id, line);
//        cout << feature_id << endl;
    }

//    cout << ids.size() << ", " << linefeatureFrame.size() << endl;
    return linefeatureFrame;
}


void LineFeatureTracker::lineMatching( vector<LineKL> &_prev_keyLine, vector<LineKL> &_curr_keyLine, Mat &_prev_descriptor,
                                      Mat &_curr_descriptor, vector<DMatch> &_good_match_vector)
{
    Ptr<BinaryDescriptorMatcher> bd_match = BinaryDescriptorMatcher::createBinaryDescriptorMatcher();
    vector<vector<DMatch> > matches_vector;
    auto prev_descriptor = _prev_descriptor.clone();
    auto curr_descriptor = _curr_descriptor.clone();

    std::vector<DMatch> matches;
    bd_match->match(prev_descriptor, curr_descriptor, matches);
    std::vector<DMatch> good_matches;
    for ( int i = 0; i < (int) matches.size(); i++ )
    {
        if(matches[i].queryIdx > _prev_keyLine.size() || matches[i].trainIdx > _curr_keyLine.size())
            continue;
        if(norm(Mat(_prev_keyLine[matches[i].queryIdx].getStartPoint()),
                Mat(_curr_keyLine[matches[i].trainIdx].getStartPoint())) > 100 ||
           norm(Mat(_prev_keyLine[matches[i].queryIdx].getEndPoint()),
                Mat(_curr_keyLine[matches[i].trainIdx].getEndPoint())) > 100)
            continue;

        good_matches.push_back( matches[i] );
    }
    _good_match_vector = good_matches;

//    cout << _good_match_vector.size() << ", " << good_matches.size() << endl;
}

LineKL MakeKeyLine( cv::Point2f start_pts, cv::Point2f end_pts, size_t cols ){
    LineKL keyLine;
    //    keyLine.class_id = 0;
    //    keyLine.numOfPixels;

    // Set start point(and octave)
    if(start_pts.x > end_pts.x)
    {
        cv::Point2f tmp_pts;
        tmp_pts = start_pts;
        start_pts = end_pts;
        end_pts = tmp_pts;
    }

    keyLine.startPointX = (int)start_pts.x;
    keyLine.startPointY = (int)start_pts.y;
    keyLine.sPointInOctaveX = start_pts.x;
    keyLine.sPointInOctaveY = start_pts.y;

    // Set end point(and octave)
    keyLine.endPointX = (int)end_pts.x;
    keyLine.endPointY = (int)end_pts.y;
    keyLine.ePointInOctaveX = end_pts.x;
    keyLine.ePointInOctaveY = end_pts.y;

    // Set angle
    keyLine.angle = atan2((end_pts.y-start_pts.y),(end_pts.x-start_pts.x));

    // Set line length & response
    keyLine.lineLength = keyLine.numOfPixels = norm( Mat(end_pts), Mat(start_pts));
    keyLine.response = norm( Mat(end_pts), Mat(start_pts))/cols;

    // Set octave
    keyLine.octave = 0;

    // Set pt(mid point)
    keyLine.pt = (start_pts + end_pts)/2;

    // Set size
    keyLine.size = fabs((end_pts.x-start_pts.x) * (end_pts.y-start_pts.y));

    return keyLine;
}

void LineFeatureTracker::lineExtraction( Mat &cur_img, vector<LineKL> &keyLine, Mat &descriptor)
{
//    line_descriptor::BinaryDescriptor::EDLineDetector;
    keyLine.clear();
    Ptr<LineBD> lineBiDes = LineBD::createBinaryDescriptor();
    Ptr<line_descriptor::LSDDetector> lineLSD = line_descriptor::LSDDetector::createLSDDetector();
    Mat keyLine_mask = Mat::ones(cur_img.size(), CV_8UC1);
//    lineLSD->detect(cur_img, keyLine, 2, 2, keyLine_mask);
    TicToc t_r;
//    lineBiDes->detect(cur_img, keyLine);
//    double t1 = t_r.toc();
    upm::Segments segs = elsed.detect(cur_img);
//    cout<< t_r.toc() << endl;
//    cout << t1 << " , " << t2 << endl;
//    Mat tmp_img1, tmp_img2;
//    cvtColor(forw_img, tmp_img1, CV_GRAY2BGR);
//    cvtColor(forw_img, tmp_img2, CV_GRAY2BGR);
//    for(int i = 0; i < forw_keyLine.size(); i++)
//        line(tmp_img1, forw_keyLine[i].getStartPoint(), forw_keyLine[i].getEndPoint(), Scalar(0,255,0), 2);
//    for(int i = 0; i < segs.size(); i++)
//    {
//        upm::Segment seg = segs[i];
//        line(tmp_img2, cv::Point2f(seg[0], seg[1]), cv::Point2f(seg[2], seg[3]), Scalar(0,255,0), 2);
//    }
//    Mat compare;
//    hconcat(tmp_img1, tmp_img2, compare);
//    imshow("1", compare);
//    waitKey(1);

    int line_id = 0;
    for(unsigned int i = 0; i < segs.size(); i++)
    {
        LineKL kl = MakeKeyLine(cv::Point2f(segs[i][0], segs[i][1]), cv::Point2f(segs[i][2], segs[i][3]), cur_img.cols);
//        if(kl.lineLength < 40)
//            continue;
        kl.class_id = line_id;
        line_id++;
        keyLine.push_back(kl);

    }
    if(keyLine.size() > 0)
       lineBiDes->compute(cur_img, keyLine, descriptor);
}

void LineFeatureTracker::normalizePoints()
{
    cur_start_un_pts.clear();
    cur_end_un_pts.clear();

    for (unsigned int i = 0; i < cur_start_pts.size(); i++)
    {
        Eigen::Vector2d start_a(cur_start_pts[i].x, cur_start_pts[i].y);
        Eigen::Vector3d start_b;
        Eigen::Vector2d end_a(cur_end_pts[i].x, cur_end_pts[i].y);
        Eigen::Vector3d end_b;

        if(model_type == "KANNALA_BRANDT")
        {
            equidistant_camera->liftProjective4line(start_a, start_b);
            equidistant_camera->liftProjective4line(end_a, end_b);
        }
        else if(model_type == "PINHOLE")
        {
            pinhole_camera->liftProjective4line(start_a, start_b);
            pinhole_camera->liftProjective4line(end_a, end_b);
        }

        cur_start_un_pts.push_back(Point2f(start_b.x() / start_b.z(), start_b.y() / start_b.z()));
        cur_end_un_pts.push_back(Point2f(end_b.x() / end_b.z(), end_b.y() / end_b.z()));
        //printf("cur pts id %d %f %f", ids[i], cur_un_pts[i].x, cur_un_pts[i].y);
    }

}

void LineFeatureTracker::imageUndistortion(camodocal::CameraPtr cam, Mat &_img, Mat &_out_undistort_img)
{
//    // camera type selected image undistortion
    Mat original_image = _img.clone();
    Mat undistorted_image;

    if(model_type == "KANNALA_BRANDT")
    {
        Mat K = cv::Mat::eye(3,3, CV_32F);
        Mat D = cv::Mat(4,1, CV_32F);

        K.at<float>(0,0) = equidistant_camera->getParameters().mu();
        K.at<float>(1,1) = equidistant_camera->getParameters().mv();
        K.at<float>(0,2) = equidistant_camera->getParameters().u0();
        K.at<float>(1,2) = equidistant_camera->getParameters().v0();

        //    D.at<float>(0,0) = 1.0;
        D.at<float>(0,0) = equidistant_camera->getParameters().k2();
        D.at<float>(1,0) = equidistant_camera->getParameters().k3();
        D.at<float>(2,0) = equidistant_camera->getParameters().k4();
        D.at<float>(3,0) = equidistant_camera->getParameters().k5();

        cv::Mat E = cv::Mat::eye(3, 3, CV_32F);
        cv::Size size = { original_image.cols, original_image.rows };
        Mat map1, map2;

    //    cam->initUndistortRectifyMap(map1, map2);
        fisheye::initUndistortRectifyMap(K, D, E, K, size, CV_16SC2, map1, map2);
        remap(original_image, undistorted_image, map1, map2, cv::INTER_LINEAR, CV_HAL_BORDER_CONSTANT);
    }
    else if(model_type == "PINHOLE")
    {
        Mat K = cv::Mat::eye(3,3, CV_32F);
        Mat D = cv::Mat(1,4, CV_32F);

        K.at<float>(0,0) = pinhole_camera->getParameters().fx();
        K.at<float>(1,1) = pinhole_camera->getParameters().fy();
        K.at<float>(0,2) = pinhole_camera->getParameters().cx();
        K.at<float>(1,2) = pinhole_camera->getParameters().cy();

        D.at<float>(0,0) = pinhole_camera->getParameters().k1();
        D.at<float>(0,1) = pinhole_camera->getParameters().k2();
        D.at<float>(0,2) = pinhole_camera->getParameters().p1();
        D.at<float>(0,3) = pinhole_camera->getParameters().p2();

        undistort(original_image, undistorted_image, K, D);
    }

//    Mat merge_img;
//    vconcat(original_image, undistorted_image, merge_img);

//    float resize_rate = 1.0;
//    cv::fisheye::undistortImage(original_image, undistorted_image, K, D);

//    Mat merge_img;
//    vconcat(original_image, undistorted_image, merge_img);
//    cv::imshow("undistorted_image", undistorted_image);
//    cv::waitKey(1);
//    cv_bridge::CvImage out_img_msg;
//    out_img_msg.encoding = sensor_msgs::image_encodings::TYPE_8UC1;
//    out_img_msg.image = merge_img;

    _out_undistort_img = undistorted_image;
}

void LineFeatureTracker::CannyDetection(Mat &src, vector<line_descriptor::KeyLine> &keylines)
{
    keylines.clear();
    float r, c;
    imageheight=src.rows; imagewidth=src.cols;

    Mat canny = src.clone();
    Canny(src, canny, 50, 50, 3);

    vector<Point2f> points;
    vector<line_descriptor::KeyLine> kls, kls_tmp, kls_tmp2;

    for(int i=0; i<src.rows;i++) {
        for(int j=0; j<src.cols;j++) {
            if( i < 5 || i > src.rows-5 || j < 5 || j > src.cols - 5)
                canny.at<unsigned char>(i,j) = 0;
        }
    }

    line_descriptor::KeyLine kl, kl1, kl2;

    for ( r = 0; r < src.rows; r++ ) {
        for ( c = 0; c < src.cols; c++ ) {
            // Find seeds - skip for non-seeds
            if ( canny.at<unsigned char>(r,c) == 0 )
                continue;

            // Found seeds
            Point2f pt;
            pt.x = c;
            pt.y = r;

            points.push_back(pt);
            canny.at<unsigned char>(pt.y, pt.x) = 0;

            int direction = 0;
            int step = 0;
            while (getPointChain( canny, pt, &pt, direction, step)) {
                points.push_back(pt);
                step++;
                canny.at<unsigned char>(pt.y, pt.x) = 0;
            }

            if ( points.size() < (unsigned int)threshold_length + 1 ) {
                points.clear();
                continue;
            }

            extractSegments( &points, &kls);

            if ( kls.size() == 0 ) {
                points.clear();
                continue;
            }

            for ( int i = 0; i < (int)kls.size(); i++ ) {
                kl = kls.at(i);
                float length = sqrt(pow((kl.startPointX - kl.endPointX),2) + pow((kl.startPointY - kl.endPointY),2));
                if(length < threshold_length) continue;
                if( (kl.startPointX <= 5.0f && kl.endPointX <= 5.0f)
                    || (kl.startPointY <= 5.0f && kl.endPointY <= 5.0f)
                    || (kl.startPointX >= imagewidth - 5.0f && kl.endPointX >= imagewidth - 5.0f)
                    || (kl.startPointY >= imageheight - 5.0f && kl.endPointY >= imageheight - 5.0f) )
                    continue;

                kls_tmp.push_back(kl);
            }

            points.clear();
            kls.clear();
        }
    }

    bool is_merged = false;
    int ith = kls_tmp.size() - 1;
    int jth = ith - 1;
    while(false)
    {
        kl1 = kls_tmp[ith];
        kl2 = kls_tmp[jth];
        is_merged = mergeSegments(&kl1, &kl2, &kl2);

        if(is_merged)
        {
            additionalOperationsOnSegments(src, &kl2);
            vector<line_descriptor::KeyLine>::iterator it = kls_tmp.begin() + ith;
            *it = kl2;
            kls_tmp.erase(kls_tmp.begin()+jth);
            ith--;
            jth = ith - 1;
        }
        else
            jth--;
        if(jth < 0)
        {
            ith--;
            jth = ith - 1;
        }
        if(ith == 1 && jth == 0)
            break;
    }

    keylines = kls_tmp;
}

bool LineFeatureTracker::getPointChain( const Mat & img, const Point2f pt, Point2f * chained_pt,
                                       int & direction, int step )
{
    float ri, ci;
    int indices[8][2]={ {1,1}, {1,0}, {1,-1}, {0,-1}, {-1,-1},{-1,0}, {-1,1}, {0,1} };

    for ( int i = 0; i < 8; i++ ) {
        ci = pt.x + indices[i][1];
        ri = pt.y + indices[i][0];

        if ( ri < 0 || ri == img.rows || ci < 0 || ci == img.cols )
            continue;

        if ( img.at<unsigned char>(ri, ci) == 0 )
            continue;

        if(step == 0) {
            chained_pt->x = ci;
            chained_pt->y = ri;
            direction = i;
            return true;
        } else {
            if(abs(i-direction) <= 2 || abs(i-direction) >= 6)
            {
                chained_pt->x = ci;
                chained_pt->y = ri;
                direction = i;
                return true;
            } else
                continue;
        }
    }
    return false;
}

void LineFeatureTracker::extractSegments( vector<Point2f> * points, vector<line_descriptor::KeyLine> * kls)
{
    bool is_line;

    int i, j;
    line_descriptor::KeyLine kl;
    Point2f ps, pe, pt;

    vector<Point2f> l_points;

    int total = points->size();

    for ( i = 0; i + threshold_length < total; i++ ) {
        ps = points->at(i);
        pe = points->at(i + threshold_length);

        double a[] = { (double)ps.x, (double)ps.y, 1 };
        double b[] = { (double)pe.x, (double)pe.y, 1 };
        double c[3], d[3];

        Mat p1 = Mat(3, 1, CV_64FC1, a).clone();
        Mat p2 = Mat(3, 1, CV_64FC1, b).clone();
        Mat p = Mat(3, 1, CV_64FC1, c).clone();
        Mat l = Mat(3, 1, CV_64FC1, d).clone();
        l = p1.cross(p2);

        is_line = true;

        l_points.clear();
        l_points.push_back(ps);

        for ( j = 1; j < threshold_length; j++ ) {
            pt.x = points->at(i+j).x;
            pt.y = points->at(i+j).y;

            p.at<double>(0,0) = (double)pt.x;
            p.at<double>(1,0) = (double)pt.y;
            p.at<double>(2,0) = 1.0;

            double dist = distPointLine( p, l );

            if ( fabs( dist ) > threshold_dist ) {
                is_line = false;
                break;
            }
            l_points.push_back(pt);
        }

        // Line check fail, test next point
        if ( is_line == false )
            continue;

        l_points.push_back(pe);

        Vec4f line;
        fitLine( Mat(l_points), line, CV_DIST_L2, 0, 0.01, 0.01);
        a[0] = line[2];
        a[1] = line[3];
        b[0] = line[2] + line[0];
        b[1] = line[3] + line[1];

        p1 = Mat(3, 1, CV_64FC1, a).clone();
        p2 = Mat(3, 1, CV_64FC1, b).clone();

        l = p1.cross(p2);

        incidentPoint( &ps, l );

        // Extending line
        for ( j = threshold_length + 1; i + j < total; j++ ) {
            pt.x = points->at(i+j).x;
            pt.y = points->at(i+j).y;

            p.at<double>(0,0) = (double)pt.x;
            p.at<double>(1,0) = (double)pt.y;
            p.at<double>(2,0) = 1.0;

            double dist = distPointLine( p, l );

            if ( fabs( dist ) > threshold_dist ) {
                j--;
                break;
            }

            pe = pt;
            l_points.push_back(pt);
        }
        fitLine( Mat(l_points), line, CV_DIST_L2, 0, 0.01, 0.01);
        a[0] = line[2];
        a[1] = line[3];
        b[0] = line[2] + line[0];
        b[1] = line[3] + line[1];

        p1 = Mat(3, 1, CV_64FC1, a).clone();
        p2 = Mat(3, 1, CV_64FC1, b).clone();

        l = p1.cross(p2);

        Point2f e1, e2;
        e1.x = ps.x;
        e1.y = ps.y;
        e2.x = pe.x;
        e2.y = pe.y;

        incidentPoint( &e1, l );
        incidentPoint( &e2, l );

        i = i + j;
        kl = MakeKeyLine(e1, e2, imagewidth);
        kl.class_id = 0;

        kls->push_back(kl);
    }
}

double LineFeatureTracker::distPointLine( const Mat & p, Mat & l )
{
    double x, y, w;

    x = l.at<double>(0,0);
    y = l.at<double>(1,0);

    w = sqrt(x*x+y*y);

    l.at<double>(0,0) = x  / w;
    l.at<double>(1,0) = y  / w;
    l.at<double>(2,0) = l.at<double>(2,0)  / w;

    return l.dot(p);
}

template<class tType>
void LineFeatureTracker::incidentPoint( tType * pt, Mat & l )
{
    double a[] = { (double)pt->x, (double)pt->y, 1.0 };
    double b[] = { l.at<double>(0,0), l.at<double>(1,0), 0.0 };
    double c[3];

    Mat xk = Mat(3, 1, CV_64FC1, a).clone();
    Mat lh = Mat(3, 1, CV_64FC1, b).clone();
    Mat lk = Mat(3, 1, CV_64FC1, c).clone();

    lk = xk.cross(lh);
    xk = lk.cross(l);

    double s = 1.0 / xk.at<double>(2,0);
    xk.convertTo(xk, -1, s);

    pt->x = (float)xk.at<double>(0,0) < 0.0f ? 0.0f : (float)xk.at<double>(0,0)
                                                                >= (imagewidth - 1.0f) ? (imagewidth - 1.0f) : (float)xk.at<double>(0,0);
    pt->y = (float)xk.at<double>(1,0) < 0.0f ? 0.0f : (float)xk.at<double>(1,0)
                                                                >= (imageheight - 1.0f) ? (imageheight - 1.0f) : (float)xk.at<double>(1,0);

}

void LineFeatureTracker::additionalOperationsOnSegments(Mat & src, line_descriptor::KeyLine * kl)
{
    if(kl->startPointX == 0.0f && kl->startPointY == 0.0f && kl->endPointX == 0.0f && kl->endPointY == 0.0f)
        return;

    double ang = (double)kl->angle;

    Point2f start = Point2f(kl->startPointX, kl->startPointY);
    Point2f end = Point2f(kl->endPointX, kl->endPointY);

    double dx = 0.0, dy = 0.0;
    dx = (double) end.x - (double) start.x;
    dy = (double) end.y - (double) start.y;

    int num_points = 10;
    Point2f *points = new Point2f[num_points];

    points[0] = start;
    points[num_points - 1] = end;
    for (int i = 0; i < num_points; i++) {
        if (i == 0 || i == num_points - 1)
            continue;
        points[i].x = points[0].x + (dx / double(num_points - 1) * (double) i);
        points[i].y = points[0].y + (dy / double(num_points - 1) * (double) i);
    }

    Point2f *points_right = new Point2f[num_points];
    Point2f *points_left = new Point2f[num_points];
    double gap = 1.0;

    for(int i = 0; i < num_points; i++) {
        points_right[i].x = cvRound(points[i].x + gap*cos(90.0 * CV_PI / 180.0 + ang));
        points_right[i].y = cvRound(points[i].y + gap*sin(90.0 * CV_PI / 180.0 + ang));
        points_left[i].x = cvRound(points[i].x - gap*cos(90.0 * CV_PI / 180.0 + ang));
        points_left[i].y = cvRound(points[i].y - gap*sin(90.0 * CV_PI / 180.0 + ang));
        pointInboardTest(src, &points_right[i]);
        pointInboardTest(src, &points_left[i]);
    }

    delete[] points; delete[] points_right; delete[] points_left;

    return;
}

void LineFeatureTracker::pointInboardTest(Mat & src, Point2f * pt)
{
    pt->x = pt->x <= 5.0f ? 5.0f : pt->x >= src.cols - 5.0f ? src.cols - 5.0f : pt->x;
    pt->y = pt->y <= 5.0f ? 5.0f : pt->y >= src.rows - 5.0f ? src.rows - 5.0f : pt->y;
}

bool LineFeatureTracker::mergeSegments( line_descriptor::KeyLine* kl1, line_descriptor::KeyLine* kl2, line_descriptor::KeyLine* kl_merged )
{
    double o[] = { 0.0, 0.0, 1.0 };
    double a[] = { 0.0, 0.0, 1.0 };
    double b[] = { 0.0, 0.0, 1.0 };
    double c[3];

    double seg1_x1 = kl1->startPointX;
    double seg1_y1 = kl1->startPointY;
    double seg1_x2 = kl1->endPointX;
    double seg1_y2 = kl1->endPointY;
    double seg2_x1 = kl2->startPointX;
    double seg2_y1 = kl2->startPointY;
    double seg2_x2 = kl2->endPointX;
    double seg2_y2 = kl2->endPointY;

    o[0] = ( seg2_x1 + seg2_x2 ) / 2.0;
    o[1] = ( seg2_y1 + seg2_y2 ) / 2.0;

    a[0] = seg1_x1;
    a[1] = seg1_y1;
    b[0] = seg1_x2;
    b[1] = seg1_y2;

    Mat ori = Mat(3, 1, CV_64FC1, o).clone();
    Mat p1 = Mat(3, 1, CV_64FC1, a).clone();
    Mat p2 = Mat(3, 1, CV_64FC1, b).clone();
    Mat l1 = Mat(3, 1, CV_64FC1, c).clone();

    l1 = p1.cross(p2);

    Point2f seg1mid, seg2mid;
    seg1mid.x = (seg1_x1 + seg1_x2) /2.0f;
    seg1mid.y = (seg1_y1 + seg1_y2) /2.0f;
    seg2mid.x = (seg2_x1 + seg2_x2) /2.0f;
    seg2mid.y = (seg2_y1 + seg2_y2) /2.0f;

    double seg1len, seg2len;
    seg1len = sqrt((seg1_x1 - seg1_x2)*(seg1_x1 - seg1_x2)+(seg1_y1 - seg1_y2)*(seg1_y1 - seg1_y2));
    seg2len = sqrt((seg2_x1 - seg2_x2)*(seg2_x1 - seg2_x2)+(seg2_y1 - seg2_y2)*(seg2_y1 - seg2_y2));

    double middist = sqrt((seg1mid.x - seg2mid.x)*(seg1mid.x - seg2mid.x) + (seg1mid.y - seg2mid.y)*(seg1mid.y - seg2mid.y));

    float angdiff = kl1->angle - kl2->angle;
    angdiff = fabs(angdiff);

    double dist = distPointLine( ori, l1 );

    if ( fabs( dist ) <= threshold_dist
        && middist <= seg1len / 2.0 + seg2len / 2.0
        && (angdiff <= CV_PI / 180.0f * 1.0f || abs(angdiff - CV_PI) <= CV_PI / 180.0f * 1.0f)) {
        mergeLines(kl1, kl2, kl2);
        return true;
    } else {
        return false;
    }
}

void LineFeatureTracker::mergeLines(line_descriptor::KeyLine * kl1, line_descriptor::KeyLine * kl2, line_descriptor::KeyLine * kl_merged)
{
    double seg1_x1 = kl1->startPointX;
    double seg1_y1 = kl1->startPointY;
    double seg1_x2 = kl1->endPointX;
    double seg1_y2 = kl1->endPointY;
    double seg2_x1 = kl2->startPointX;
    double seg2_y1 = kl2->startPointY;
    double seg2_x2 = kl2->endPointX;
    double seg2_y2 = kl2->endPointY;

    double xg = 0.0, yg = 0.0;
    double delta1x = 0.0, delta1y = 0.0, delta2x = 0.0, delta2y = 0.0;
    float ax = 0, bx = 0, cx = 0, dx = 0;
    float ay = 0, by = 0, cy = 0, dy = 0;
    double li = 0.0, lj = 0.0;
    double thi = 0.0, thj = 0.0, thr = 0.0;
    double axg = 0.0, bxg = 0.0, cxg = 0.0, dxg = 0.0, delta1xg = 0.0, delta2xg = 0.0;

    ax = seg1_x1;
    ay = seg1_y1;
    bx = seg1_x2;
    by = seg1_y2;
    cx = seg2_x1;
    cy = seg2_y1;
    dx = seg2_x2;
    dy = seg2_y2;

    float dlix = (bx - ax);
    float dliy = (by - ay);
    float dljx = (dx - cx);
    float dljy = (dy - cy);

    li = sqrt((double) (dlix * dlix) + (double) (dliy * dliy));
    lj = sqrt((double) (dljx * dljx) + (double) (dljy * dljy));

    xg = (li * (double) (ax + bx) + lj * (double) (cx + dx))
         / (double) (2.0 * (li + lj));
    yg = (li * (double) (ay + by) + lj * (double) (cy + dy))
         / (double) (2.0 * (li + lj));

    if(dlix == 0.0f) thi = CV_PI / 2.0;
    else thi = atan(dliy / dlix);

    if(dljx == 0.0f) thj = CV_PI / 2.0;
    else thj = atan(dljy / dljx);

    if (fabs(thi - thj) <= CV_PI / 2.0)
    {
        thr = (li * thi + lj * thj) / (li + lj);
    }
    else
    {
        double tmp = thj - CV_PI * (thj / fabs(thj));
        thr = li * thi + lj * tmp;
        thr /= (li + lj);
    }

    axg = ((double) ay - yg) * sin(thr) + ((double) ax - xg) * cos(thr);

    bxg = ((double) by - yg) * sin(thr) + ((double) bx - xg) * cos(thr);

    cxg = ((double) cy - yg) * sin(thr) + ((double) cx - xg) * cos(thr);

    dxg = ((double) dy - yg) * sin(thr) + ((double) dx - xg) * cos(thr);

    delta1xg = min(axg,min(bxg,min(cxg,dxg)));
    delta2xg = max(axg,max(bxg,max(cxg,dxg)));

    delta1x = delta1xg * cos(thr) + xg;
    delta1y = delta1xg * sin(thr) + yg;
    delta2x = delta2xg * cos(thr) + xg;
    delta2y = delta2xg * sin(thr) + yg;

    line_descriptor::KeyLine kl_tmp = MakeKeyLine(Point2f(delta1x, delta1y), Point2f(delta2x, delta2y), imagewidth);
    kl_tmp.class_id = kl2->class_id;
    *kl_merged = kl_tmp;
}

double LineFeatureTracker::Union_dist(VectorXd a, VectorXd b)
{
    double d = 0;
    if(a.size() == b.size())
        for(int i = 0; i < a.size(); i++)
            if(a(i) || b(i))
                d++;
    return d;
}

double LineFeatureTracker::Intersection_dist(VectorXd a, VectorXd b)
{
    double d = 0;
    if(a.size() == b.size())
        for(int i = 0; i < a.size(); i++)
            if(a(i) && b(i))
                d++;
    return d;
}

VectorXd LineFeatureTracker::Union(VectorXd a, VectorXd b)
{
    VectorXd d(a.size());
    if(a.size() == b.size())
        for(int i = 0; i < a.size(); i++)
            d(i) = (a(i) || b(i));

    return d;
}

VectorXd LineFeatureTracker::Intersection(VectorXd a, VectorXd b)
{
    VectorXd d(a.size());
    if(a.size() == b.size())
        for(int i = 0; i < a.size(); i++)
            d(i) = (a(i) && b(i));

    return d;
}

void LineFeatureTracker::getVPHypVia2Lines(vector<KeyLine> cur_keyLine, vector<Vector3d> &para_vector, vector<double> &length_vector, vector<double> &orientation_vector, std::vector<std::vector<Vector3d> > &vpHypo )
{
    int num = cur_keyLine.size();

    double noiseRatio = 0.5;
    double p = 1.0 / 3.0 * pow( 1.0 - noiseRatio, 2 );

    double confEfficience = 0.9999;
    int it = log( 1 - confEfficience ) / log( 1.0 - p );

    int numVp2 = 360;
    double stepVp2 = 2.0 * CV_PI / numVp2;

    // get the parameters of each line
    for ( int i = 0; i < num; ++i )
    {
        Vector3d p1(cur_keyLine[i].getStartPoint().x, cur_keyLine[i].getStartPoint().y, 1.0);
        Vector3d p2(cur_keyLine[i].getEndPoint().x, cur_keyLine[i].getEndPoint().y, 1.0);

        para_vector.push_back(p1.cross( p2 ));

        double dx = cur_keyLine[i].getEndPoint().x - cur_keyLine[i].getStartPoint().x;
        double dy = cur_keyLine[i].getEndPoint().y - cur_keyLine[i].getStartPoint().y;
        length_vector.push_back(sqrt( dx * dx + dy * dy ));

        double orientation = atan2( dy, dx );
        if ( orientation < 0 )
        {
            orientation += CV_PI;
        }
        orientation_vector.push_back(orientation);
    }

    // get vp hypothesis for each iteration
    vpHypo = std::vector<std::vector<Vector3d> > ( it * numVp2, std::vector<Vector3d>(4) );
    int count = 0;
    srand((unsigned)time(NULL));
    for ( int i = 0; i < it; ++i )
    {
        int idx1 = rand() % num;
        int idx2 = rand() % num;

        while ( idx2 == idx1 )
        {
            idx2 = rand() % num;
        }
        // get the vp1
        Vector3d vp1_Img = para_vector[idx1].cross( para_vector[idx2] );
        if (vp1_Img(2) == 0)
        {
            i --;
            continue;
        }
        Vector3d vp1;
        if(model_type == "KANNALA_BRANDT")
            vp1 = Vector3d(vp1_Img(0) / vp1_Img(2) - equidistant_camera->getParameters().mu(),
                           vp1_Img(1) / vp1_Img(2) - equidistant_camera->getParameters().mv(),
                           equidistant_camera->getParameters().u0());
        else if(model_type == "PINHOLE")
        {
            vp1 = Vector3d(vp1_Img(0) / vp1_Img(2) - pinhole_camera->getParameters().cx(),
                           vp1_Img(1) / vp1_Img(2) - pinhole_camera->getParameters().cy(),
                           pinhole_camera->getParameters().fx() );
        }
        if ( vp1(2) == 0 ) { vp1(2) = 0.0011; }
        double N = sqrt( vp1(0) * vp1(0) + vp1(1) * vp1(1) + vp1(2) * vp1(2) );
        vp1 *= 1.0 / N;

        // get the vp2 and vp3
        Vector3d vp2( 0.0, 0.0, 0.0 );
        Vector3d vp3( 0.0, 0.0, 0.0 );
        Vector3d vp4( 0.0, 0.0, 0.0 );

        for ( int j = 0; j < numVp2; ++ j )
        {
            // vp2
            double lambda = j * stepVp2;

            double k1 = vp1(0) * sin( lambda ) + vp1(1) * cos( lambda );
            double k2 = vp1(2);
            double phi = atan( - k2 / k1 );

            double Z = cos( phi );
            double X = sin( phi ) * sin( lambda );
            double Y = sin( phi ) * cos( lambda );

            vp2(0) = X;  vp2(1) = Y;  vp2(2) = Z;
            if ( vp2(2) == 0.0 ) { vp2(2) = 0.0011; }
            N = sqrt( vp2(0) * vp2(0) + vp2(1) * vp2(1) + vp2(2) * vp2(2) );
            vp2 *= 1.0 / N;
            if ( vp2(2) < 0 ) { vp2 *= -1.0; }

            // vp3
            vp3 = vp1.cross( vp2 );
            if ( vp3(2) == 0.0 ) { vp3(2) = 0.0011; }
            N = sqrt( vp3(0) * vp3(0) + vp3(1) * vp3(1) + vp3(2) * vp3(2) );
            vp3 *= 1.0 / N;
            if ( vp3(2) < 0 ) { vp3 *= -1.0; }
            //
            vpHypo[count][0] = Vector3d( vp1(0), vp1(1), vp1(2) );
            vpHypo[count][1] = Vector3d( vp2(0), vp2(1), vp2(2) );
            vpHypo[count][2] = Vector3d( vp3(0), vp3(1), vp3(2) );

            count ++;
        }
    }
    return;
}

void LineFeatureTracker::getSphereGrids(vector<KeyLine> cur_keyLine, vector<Vector3d> &para_vector, vector<double> &length_vector, vector<double> &orientation_vector, std::vector<std::vector<double> > &sphereGrid )
{
    // build sphere grid with 1 degree accuracy
    double angelAccuracy = 1.0 / 180.0 * CV_PI;
    double angleSpanLA = CV_PI / 2.0;
    double angleSpanLO = CV_PI * 2.0;
    int gridLA = angleSpanLA / angelAccuracy;
    int gridLO = angleSpanLO / angelAccuracy;

    sphereGrid = std::vector< std::vector<double> >( gridLA, std::vector<double>(gridLO) );
    for ( int i=0; i<gridLA; ++i )
    {
        for ( int j=0; j<gridLO; ++j )
        {
            sphereGrid[i][j] = 0.0;
        }
    }

    // put intersection points into the grid
    double angelTolerance = 60.0 / 180.0 * CV_PI;
    Vector3d ptIntersect;
    double x = 0.0, y = 0.0;
    double X = 0.0, Y = 0.0, Z = 0.0, N = 0.0;
    double latitude = 0.0, longitude = 0.0;
    int LA = 0, LO = 0;
    double angleDev = 0.0;
    for ( int i=0; i<cur_keyLine.size()-1; ++i )
    {
        for ( int j=i+1; j<cur_keyLine.size(); ++j )
        {
            ptIntersect = para_vector[i].cross( para_vector[j] );

            if ( ptIntersect(2) == 0 )
            {
                continue;
            }

            x = ptIntersect(0) / ptIntersect(2);
            y = ptIntersect(1) / ptIntersect(2);

            if(model_type == "KANNALA_BRANDT")
            {
                X = x - equidistant_camera->getParameters().u0();
                Y = y - equidistant_camera->getParameters().v0();
                Z = equidistant_camera->getParameters().mu();
            }
            else if(model_type == "PINHOLE")
            {
                X = x - pinhole_camera->getParameters().cx();
                Y = y - pinhole_camera->getParameters().cy();
                Z = pinhole_camera->getParameters().fx();
            }
            N = sqrt( X * X + Y * Y + Z * Z );

            latitude = acos( Z / N );
            longitude = atan2( X, Y ) + CV_PI;

            LA = int( latitude / angelAccuracy );
            if ( LA >= gridLA )
            {
                LA = gridLA - 1;
            }

            LO = int( longitude / angelAccuracy );
            if ( LO >= gridLO )
            {
                LO = gridLO - 1;
            }

            //
            angleDev = abs( orientation_vector[i] - orientation_vector[j] );
            angleDev = min( CV_PI - angleDev, angleDev );
            if ( angleDev > angelTolerance )
            {
                continue;
            }

            sphereGrid[LA][LO] += sqrt( length_vector[i] * length_vector[j] ) * ( sin( 2.0 * angleDev ) + 0.2 ); // 0.2 is much robuster
        }
    }

    //
    int halfSize = 1;
    int winSize = halfSize * 2 + 1;
    int neighNum = winSize * winSize;

    // get the weighted line length of each grid
    std::vector< std::vector<double> > sphereGridNew = std::vector< std::vector<double> >( gridLA, std::vector<double>(gridLO) );
    for ( int i=halfSize; i<gridLA-halfSize; ++i )
    {
        for ( int j=halfSize; j<gridLO-halfSize; ++j )
        {
            double neighborTotal = 0.0;
            for ( int m=0; m<winSize; ++m )
            {
                for ( int n=0; n<winSize; ++n )
                {
                    neighborTotal += sphereGrid[i-halfSize+m][j-halfSize+n];
                }
            }

            sphereGridNew[i][j] = sphereGrid[i][j] + neighborTotal / neighNum;
        }
    }
    sphereGrid = sphereGridNew;
}

void LineFeatureTracker::getBestVpsHyp( std::vector<std::vector<double> > &sphereGrid, std::vector<std::vector<Vector3d> >  &vpHypo, std::vector<Vector3d> &vps  )
{
    int num = vpHypo.size();
    double oneDegree = 1.0 / 180.0 * CV_PI;

    // get the corresponding line length of every hypotheses
    std::vector<double> lineLength( num, 0.0 );
    for ( int i = 0; i < num; ++ i )
    {
        std::vector<cv::Point2d> vpLALO( 3 );
        for ( int j = 0; j < 3; ++ j )
        {
            if ( vpHypo[i][j](2) == 0.0 )
            {
                continue;
            }

            if ( vpHypo[i][j](2) > 1.0 || vpHypo[i][j](2) < -1.0 )
            {
                cout<<1.0000<<endl;
            }
            double latitude = acos( vpHypo[i][j](2) );
            double longitude = atan2( vpHypo[i][j](0), vpHypo[i][j](1) ) + CV_PI;

            int gridLA = int( latitude / oneDegree );
            if ( gridLA == 90 )
            {
                gridLA = 89;
            }

            int gridLO = int( longitude / oneDegree );
            if ( gridLO == 360 )
            {
                gridLO = 359;
            }

            lineLength[i] += sphereGrid[gridLA][gridLO];
        }
    }

    // get the best hypotheses
    int bestIdx = 0;
    double maxLength = 0.0;
    for ( int i = 0; i < num; ++ i )
    {
        if ( lineLength[i] > maxLength )
        {
            maxLength = lineLength[i];
            bestIdx = i;
        }
    }

    vps = vpHypo[bestIdx];
//    cout << vps.size() << endl;
}

void LineFeatureTracker::lines2Vps(vector<KeyLine> cur_keyLine, double thAngle, std::vector<Vector3d> &vps, std::vector<std::vector<int> > &clusters, vector<int> &vp_idx)
{
    clusters.clear();
    clusters.resize( 3 );

    int vps_size = 3;
    //get the corresponding vanish points on the image plane
    std::vector<cv::Point2d> vp2D( vps_size );
    for ( int i = 0; i < vps_size; ++ i )
    {
        if(model_type == "KANNALA_BRANDT")
        {
            vp2D[i].x =  vps[i](0) * equidistant_camera->getParameters().mu() /
                         vps[i](2) + equidistant_camera->getParameters().u0();
            vp2D[i].y =  vps[i](1) * equidistant_camera->getParameters().mv() /
                         vps[i](2) + equidistant_camera->getParameters().v0();
        }
        else if(model_type == "PINHOLE")
        {
            vp2D[i].x =  vps[i](0) * pinhole_camera->getParameters().fx() /
                         vps[i](2) + pinhole_camera->getParameters().cx();
            vp2D[i].y =  vps[i](1) * pinhole_camera->getParameters().fy() /
                         vps[i](2) + pinhole_camera->getParameters().cy();
        }
    }

    for ( int i = 0; i < cur_keyLine.size(); ++ i )
    {
        double x1 = cur_keyLine[i].getStartPoint().x;
        double y1 = cur_keyLine[i].getStartPoint().y;
        double x2 = cur_keyLine[i].getEndPoint().x;
        double y2 = cur_keyLine[i].getEndPoint().y;
        double xm = ( x1 + x2 ) / 2.0;
        double ym = ( y1 + y2 ) / 2.0;

        double v1x = x1 - x2;
        double v1y = y1 - y2;
        double N1 = sqrt( v1x * v1x + v1y * v1y );
        v1x /= N1;   v1y /= N1;

        double minAngle = 1000.0;
        int bestIdx = 0;
        for ( int j = 0; j < vps_size; ++ j )
        {
            double v2x = vp2D[j].x - xm;
            double v2y = vp2D[j].y - ym;
            double N2 = sqrt( v2x * v2x + v2y * v2y );
            v2x /= N2;  v2y /= N2;

            double crossValue = v1x * v2x + v1y * v2y;
            if ( crossValue > 1.0 )
            {
                crossValue = 1.0;
            }
            if ( crossValue < -1.0 )
            {
                crossValue = -1.0;
            }
            double angle = acos( crossValue );
            angle = min( CV_PI - angle, angle );

            if ( angle < minAngle )
            {
                minAngle = angle;
                bestIdx = j;
            }
        }

        //
        if ( minAngle < thAngle )
        {
            clusters[bestIdx].push_back( i );
            vp_idx.push_back(bestIdx);
        }
        else
            vp_idx.push_back(3);
    }
}

void LineFeatureTracker::drawClusters( cv::Mat &img, std::vector<KeyLine> &lines, std::vector<std::vector<int> > &clusters )
{
    Mat vp_img = img.clone();
    Mat line_img = img.clone();
    int cols = img.cols;
    int rows = img.rows;

    //draw lines
    std::vector<cv::Scalar> lineColors( 3 );
    lineColors[0] = cv::Scalar( 0, 0, 255 );
    lineColors[1] = cv::Scalar( 0, 255, 0 );
    lineColors[2] = cv::Scalar( 255, 0, 0 );
//    lineColors[3] = cv::Scalar( 0, 255, 255 );

    for ( int i=0; i<lines.size(); ++i )
    {
        int idx = i;
        cv::Point2f pt_s = lines[i].getStartPoint();
        cv::Point2f pt_e = lines[i].getEndPoint();
        cv::Point pt_m = ( pt_s + pt_e ) * 0.5;

        cv::line( vp_img, pt_s, pt_e, cv::Scalar(0,0,0), 2, CV_AA );
        cv::line( line_img, pt_s, pt_e, cv::Scalar(0,255,255), 2, CV_AA );
    }

    for ( int i = 0; i < clusters.size(); ++i )
    {
        for ( int j = 0; j < clusters[i].size(); ++j )
        {
            int idx = clusters[i][j];

            cv::Point2f pt_s = lines[idx].getStartPoint();
            cv::Point2f pt_e = lines[idx].getEndPoint();
            cv::Point pt_m = ( pt_s + pt_e ) * 0.5;

            cv::line( vp_img, pt_s, pt_e, lineColors[i], 2, CV_AA );
        }
    }
    imshow("img", img);
    imshow("line img", line_img);
    imshow("vp img", vp_img);
    waitKey(1);
}

double LineFeatureTracker::SafeAcos (double x)
{
    if (x < -1.0) x = -1.0 ;
    else if (x > 1.0) x = 1.0 ;
    return acos(x) ;
}

void LineFeatureTracker::removeRow(Eigen::MatrixXd& matrix, unsigned int rowToRemove)
{
    unsigned int numRows = matrix.rows()-1;
    unsigned int numCols = matrix.cols();
    if( rowToRemove < numRows )
        matrix.block(rowToRemove,0,numRows-rowToRemove,numCols) = matrix.block(rowToRemove+1,0,numRows-rowToRemove,numCols);
    matrix.conservativeResize(numRows,numCols);
}

void LineFeatureTracker::removeColumn(Eigen::MatrixXd& matrix, unsigned int colToRemove)
{
    unsigned int numRows = matrix.rows();
    unsigned int numCols = matrix.cols()-1;
    if( colToRemove < numCols )
        matrix.block(0,colToRemove,numRows,numCols-colToRemove) = matrix.block(0,colToRemove+1,numRows,numCols-colToRemove);
    matrix.conservativeResize(numRows,numCols);
}

void LineFeatureTracker::reduceVector(vector<Point2f> &v, vector<uchar> status)
{
    int j = 0;
    for (int i = 0; i < int(v.size()); i+=2)
        if (status[i] && status[i+1]){
            v[j++] = v[i];
            v[j++] = v[i+1];
        }
    v.resize(j);
}

void LineFeatureTracker::readIntrinsicParameter(const vector<string> &calib_file)
{
    cv::FileStorage fs( calib_file[0], cv::FileStorage::READ );
    fs["model_type"] >> model_type;
    if (model_type == "KANNALA_BRANDT")
    {
        EquidistantCameraPtr camera(new EquidistantCamera);
        EquidistantCamera::Parameters params = camera->getParameters();
        params.readFromYamlFile(calib_file[0]);
        camera->setParameters(params);
        equidistant_camera = camera;
    }
    else if (model_type == "PINHOLE")
    {
        PinholeCameraPtr camera(new PinholeCamera);
        PinholeCamera::Parameters params = camera->getParameters();
        params.readFromYamlFile(calib_file[0]);
        camera->setParameters(params);
        pinhole_camera = camera;
    }
    for (size_t i = 0; i < calib_file.size(); i++)
    {
        ROS_INFO("reading paramerter of camera %s", calib_file[i].c_str());
        camodocal::CameraPtr camera = CameraFactory::instance()->generateCameraFromYamlFile(calib_file[i]);
        m_camera.push_back(camera);
    }
    if (calib_file.size() == 2)
        stereo_cam = 1;
}

void LineFeatureTracker::drawTrack(const Mat img, vector<int> ids, vector<Point2f> start_pts, vector<Point2f> end_pts)
{
    imTrack = img.clone();
    cv::cvtColor(imTrack, imTrack, CV_GRAY2RGB);
    for (size_t j = 0; j < start_pts.size(); j++)
    {
        double len = std::min(1.0, 1.0 * track_cnt[j] / 20);
        cv::line(imTrack, start_pts[j], end_pts[j], cv::Scalar(255 * (1 - len), 0, 255 * len), 2);
    }
}

cv::Mat LineFeatureTracker::getTrackImage()
{
    return imTrack;
}

void LineFeatureTracker::updateID()
{
    for(int i = 0; i < ids.size(); i++)
    {
        if(ids[i] == -1)
            ids[i] = n_id++;
    }
}
